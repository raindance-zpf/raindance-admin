<?php
/**
 * Created by PhpStorm.
 * User: raindance
 * Date: 2019/1/21
 * Time: 16:39
 */

return [
    'create' => '创建',
    'add' => '添加',
    'bulk_delete' => '批量删除',
    'search' => '查询',
    'dashboard' => '面板',
    'close' => '关闭',
    'create_menu' => '创建菜单',
    'alter_menu' => '修改菜单',
    'confirm' => '确定',
    'cancel' => '取消'
];