## 项目介绍

基于Laravel、Vue、AdminLTE UI 构建后台管理框架

## 环境要求
- PHP >= 7.1.0
- Laravel >= 5.5.0
- Fileinfo PHP Extension

## 安装
使用Git从仓库下载代码
```
https://git.dev.tencent.com/raindance/raindance-admin.git
```
使用composer install 进行安装包
```
composer install --no-dev
```
运行database/raindance-admin.sql 写入数据

通过以下命令发布资源和配置文件
```
php artisan vendor:publish --provider="Raindance\RainAdmin\RainAdminServiceProvider"
```
单独发布配置文件
```
php artisan vendor:publish --tag="rain-admin-config"
```
单独发布lang文件

```
php artisan vendor:publish --tag="rain-admin-lang"
```

单独发布资源文件

```
php artisan vendor:publish --tag="rain-admin-assets"
```
单独发布Errors资源文件

```
php artisan vendor:publish --tag="rain-admin-errors"
```
登录信息
````
- 账户： admin
- 密码：111111
```

## 配置

文件config/admin.php包含一系列配置，您可以在其中找到默认配置。

## 权限管理文档

[spatie/laravel-permission](https://github.com/spatie/laravel-permission)

## Vue

[查看文档](https://cn.vuejs.org/v2/guide/)

## ivivew UI框架

[查看文档](https://www.iviewui.com/)

## axios 网络请求

[查看文档](https://github.com/axios/axios)

## AdminLTE UI框架

[查看文档](https://adminlte.io/themes/AdminLTE/documentation/index.html)

## 扩展包


| 包名 | 描述 | 版本 |
| :------:| :------: | :------: |
| spatie/laravel-permission | 权限管理 | ^2.29 |
| kalnoy/nestedset | 嵌套Model Set | ^4.3 |
| arcanedev/log-viewer | 日志管理 | 4.4.* |