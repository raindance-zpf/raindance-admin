<?php

namespace Raindance\RainAdmin\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class AdminLog extends Model
{

    protected $fillable = [
        'causer_id',
        'causer_type',
        'path',
        'method',
        'ip',
        'subject',
        'subject_id',
        'log_name',
        'description',
        'properties'
    ];
    protected $casts = [
        'properties' => 'collection',
    ];

    public function causer(): MorphTo
    {
        return $this->morphTo();
    }

    public function subject(): MorphTo
    {
        return $this->morphTo();
    }
}
