<?php

namespace Raindance\RainAdmin\Models;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

class Dictionary extends Model
{

    use NodeTrait;
    protected $fillable = [
        'parent_id',
        'lft',
        'rgt',
        'dict_name',
        'dict_label',
        'dict_value',
        'dict_type',
        'remark',
        'sort_order',
        'is_system',
        'status'
    ];
    protected $casts = [
        'status' => 'boolean'
    ];

    public function getLftName()
    {
        return 'lft';
    }

    public function getRgtName()
    {
        return 'rgt';
    }

    public function getParentIdName()
    {
        return 'parent_id';
    }

    // Specify parent id attribute mutator
    public function setParentAttribute($value)
    {
        $this->setParentIdAttribute($value);
    }
}
