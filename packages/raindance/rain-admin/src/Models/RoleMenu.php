<?php

namespace Raindance\RainAdmin\Models;

use Illuminate\Database\Eloquent\Model;

class RoleMenu extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'role_id',
        'menu_id'
    ];
}
