<?php
/**
 * RainAdmin user Model
 */
namespace Raindance\RainAdmin\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User;
use Illuminate\Notifications\Notifiable;
use Raindance\RainAdmin\Observers\LogObserve;
use Spatie\Permission\Traits\HasRoles;
use Webpatser\Uuid\Uuid;

class AdminUser extends User
{
    protected $guard_name = 'admin';
    use Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','uuid', 'mobile', 'avatar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->uuid = static::getUUID();
        });
        static::observe(LogObserve::class);
    }

    /**
     * Generate UUID
     * @return string
     * @throws \Exception
     */
    public static function getUUID ()
    {
        $string = (string) Uuid::generate(4);
        $string = str_replace("-", "", $string);
        if (static::query()->where('uuid', $string)->exists()) {
            static::getUUID();
        }
        return $string;
    }

    /**
     * Create a user
     * @param array $data
     * @return mixed
     */
    public static function createUser (array $data)
    {
        $result = static::create([
            'name' => $data['name'],
            'password' => bcrypt($data['password'])
        ]);
        return $result;
    }

    /**
     * Update user
     * @param AdminUser $user
     * @param array $data
     * @return bool
     */
    public static function updateUser (AdminUser $user, array $data)
    {
        if (!is_null(array_get($data, 'password'))) {
            $data['password'] = bcrypt($data['password']);
        } else  {
            array_forget($data, 'password');
        }
        return $user->update($data);
    }

    /**
     * 用户添加角色
     * @param AdminUser $user
     * @param array $roles
     */
    public static function attachRole (AdminUser $user, array $roles)
    {
        $roleModels = Role::whereIn('id', $roles)->get();
        $user->syncRoles($roleModels);
    }

    /**
     * 用户添加权限
     * @param AdminUser $user
     * @param array $permission
     */
    public static function attachPermission (AdminUser $user, array $permission)
    {
        $perms = Permission::whereIn('id', $permission)->get();
        $user->syncPermissions($perms);
    }
}
