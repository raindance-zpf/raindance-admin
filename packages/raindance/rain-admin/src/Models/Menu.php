<?php
/**
 * Menu Model
 */
namespace Raindance\RainAdmin\Models;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;
use Raindance\RainAdmin\Observers\LogObserve;

class Menu extends Model
{
    use NodeTrait;
    public static $options = [];
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

    }

    protected $fillable = [
        'title',
        'url',
        'target',
        'icon_class',
        'color',
        'parent_id',
        'sort_order',
        'route',
        'parameters',
        'lft',
        'rgt'
    ];
    public function getLftName()
    {
        return 'lft';
    }

    public function getRgtName()
    {
        return 'rgt';
    }

    public function getParentIdName()
    {
        return 'parent_id';
    }

    // Specify parent id attribute mutator
    public function setParentAttribute($value)
    {
        $this->setParentIdAttribute($value);
    }
    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->url = empty($model->url) ? '#' : $model->url;
        });
        static::observe(LogObserve::class);
    }

    public static function traverse ($categories, $prefix = '&nbsp;')
    {

        foreach ($categories as $category) {
            $category->title = str_repeat($prefix, 2) . $category->title;
            static::$options[] = [
                'id' => $category->id,
                'title' => $category->title
            ];
            if (!is_null($category->children) && count($category->children)) {
                static::traverse($category->children, str_repeat($prefix, 2));
            }
        }
        return collect(static::$options)->prepend(['id' => 0, 'title' => '&nbsp;&nbsp;Root'])->all();
    }


    /**
     * Transform menus
     * @param $menus
     * @param array $menuIds
     * @return mixed
     */
    public static function menuTransformChecked ($menus, array $menuIds)
    {
        foreach ($menus as $menu) {
            if (in_array($menu->id, $menuIds)) {
                $menu->checked = true;
            } else {
                $menu->checed = false;
            }
            if (!is_null($menu->children)) {
                static::menuTransformChecked($menu->children, $menuIds);
            }
        }
        return $menus;
    }

    /**
     * Menu tree set
     * @return mixed
     */
    public static function menuTreeSet ()
    {
        /*$menus  = RoleMenu::whereIn('role_id', current_user()->roles()->pluck('id'))
            ->pluck('menu_id')
            ->unique()
            ->values()
            ->all();*/
        return static::orderBy('sort_order')
            //->whereIn('id', $menus)
            ->get()
            ->toTree();
    }
    public function roles ()
    {
        return $this->belongsToMany(Role::class, 'role_menus', 'menu_id', 'role_id');
    }

}
