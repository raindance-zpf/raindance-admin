<?php

namespace Raindance\RainAdmin\Models;

use Illuminate\Database\Eloquent\Model;
use Raindance\RainAdmin\Facades\RainAdmin;
use Raindance\RainAdmin\Observers\LogObserve;

class Role extends \Spatie\Permission\Models\Role
{
    public static function boot()
    {
        parent::boot();
        static::observe(LogObserve::class);
    }
    public static function setSyncPermissionTo (Role $role, array $permIds)
    {
        $permissions = Permission::whereIn('id', $permIds)->get();
        $role->syncPermissions($permissions);
    }
    public function menus ()
    {
        return $this->belongsToMany(Menu::class, 'role_menus', 'role_id', 'menu_id');
    }

    /**
     * 保存关联菜单
     * @param Role $role
     * @param array $menus
     */
    public static function saveToMenu (Role $role, array $menus)
    {
        $role->menus()->detach();
        $role->menus()->attach($menus);
    }

    /**
     * Role has menu
     * @param $menuId
     * @return bool
     */
    public static function roleHasMenu ($menuId)
    {
        if (!is_null($user = RainAdmin::user())) {
            $menus  = RoleMenu::whereIn('role_id', $user->roles()->pluck('id'))
                ->pluck('menu_id')
                ->unique()
                ->values()
                ->all();
            return in_array($menuId, $menus);
        }
        return false;
    }
}
