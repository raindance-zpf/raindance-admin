<?php

namespace Raindance\RainAdmin\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Raindance\RainAdmin\Facades\RainAdmin;

class Permission extends \Spatie\Permission\Models\Permission
{

    /**
     * If current user is administrator.
     *
     * @return mixed
     */
    public static function isAdministrator($id = null)
    {
        $user = is_null($id) ? auth()->guard('admin')->user() : AdminUser::find($id);
        return !is_null($user) && $user->hasRole('super-admin')
            && object_get($user, 'id') === 1;
    }
}
