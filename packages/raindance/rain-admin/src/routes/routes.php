<?php
/**
 * Created by PhpStorm.
 * User: raindance
 * Date: 2019/3/8
 * Time: 9:59
 */

Route::namespace(config('admin.route.namespace'))
    ->prefix(config('admin.route.prefix'))
    ->group(function () {

    Route::redirect('/', '/admin/dashboard', 301);
    Route::redirect('/admin', '/admin/dashboard', 301);


    Route::get('/dashboard', 'DashboardController@index')->name('admin.dashboard');
    /**
     * Login and Logout
     */
    Route::get('/login', 'LoginController@showLoginForm')->name('admin.showLogin');
    Route::post('/login', 'LoginController@login')->name('admin.postLogin');
    Route::get('logout', 'LoginController@logout')->name('admin.logout');

    Route::get('roles/selectOptions', function (Raindance\RainAdmin\Models\Role $role) {
        return \Raindance\RainAdmin\Services\ResponseUtil::success($role->select(['slug', 'id'])->get());
    })->name('roles.selectOptions');
    /**
     * Role
     */
    Route::get('roles/all', 'RolesController@all')->name('roles.all');  //所有角色
    Route::resource('roles', 'RolesController');
    /**
     * Permission
     */
    Route::get('permissions/all', function (\Spatie\Permission\Models\Permission $permission) {
        return \Raindance\RainAdmin\Services\ResponseUtil::success(\Raindance\RainAdmin\Resources\PermissionWithKey::collection($permission::all()));
    })->name('permissions.all');
    Route::get('permissions/withPagination', 'PermissionsController@withPagination')->name('permissions.withPagination');  //所有权限分页
    Route::resource('permissions', 'PermissionsController');

    /**
     * 用户
     */
    Route::get('users/all', 'UsersController@all')->name('users.all');   //User lists
    Route::resource('users', 'UsersController');
    /**
     * 菜单
     */
    Route::get('menus/treeData', 'MenusController@treeData')->name('menus.treeData');  //menu tree
    Route::get('menus/selectOptions', 'MenusController@selectOptions')->name('menus.selectOptions');  //list for select options
    Route::resource('menus', 'MenusController');
    /**
     * 日志
     */
    Route::get('logs/all', 'LogsController@all')->name('logs.all');
    Route::resource('logs', 'LogsController')->only(['index', 'destroy']);
    Route::get('log-viewer', 'LogviewerController@index')->name('logs.viewer');
    /**
     * 个人信息
     */
    Route::get('profile', 'ProfileController@index')->name('profile.index');
    Route::get('profile/info', 'ProfileController@info')->name('profile.info');
    Route::put('profile/update', 'ProfileController@update')->name('profile.update');

    /**
     * 上传
     */
    Route::post('upload/image', 'UploadController@uploadImage')->name('upload.image');   //上传图片
    Route::put('upload/remove', 'UploadController@remove')->name('upload.remove');   //删除图片
    /**
     * 字典
     */
    Route::get('dict/all', 'DictionaryController@all')->name('dict.all');
    Route::resource('dict', 'DictionaryController');
    Route::get('dictValue/all', 'DictValueController@all')->name('dictValue.all');
    Route::resource('dictValue', 'DictValueController');

});