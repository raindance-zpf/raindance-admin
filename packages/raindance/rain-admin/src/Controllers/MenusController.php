<?php

namespace Raindance\RainAdmin\Controllers;


use Illuminate\Support\Facades\DB;
use Raindance\RainAdmin\Constants\CommonConst;
use Raindance\RainAdmin\Models\Menu;
use Raindance\RainAdmin\Models\RoleMenu;
use Raindance\RainAdmin\Resources\MenuTreeResource;
use Raindance\RainAdmin\Services\ResponseUtil;
use Illuminate\Http\Request;

class MenusController extends Controller
{

    public function __construct()
    {
        $this->middleware(['web', 'admin', 'admin.role:super-admin']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $treeData = Menu::orderBy('sort_order')->get()->toTree();
        return view('admin::menu.index', compact('treeData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (empty($request->get('title'))) {
            return ResponseUtil::failed('输入标题', 200);
        }
        Menu::create($request->all());
        return ResponseUtil::success();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return ResponseUtil::success(Menu::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (empty($request->get('title'))) {
            return ResponseUtil::failed('输入标题', 200);
        }
        Menu::find($id)->update($request->all());
        return ResponseUtil::success();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $model = Menu::findOrFail($id);
            $model->roles()->detach();
            $model->delete();
            DB::commit();
            return ResponseUtil::success();
        } catch (\Exception $exception) {
            DB::rollBack();
            return ResponseUtil::failed(CommonConst::DELETE_FAILED, 200);
        }

    }

    /**
     * Tree set for select options
     * @return mixed
     */
    public function selectOptions ()
    {
        return ResponseUtil::success(Menu::traverse(Menu::get()->toTree()));
    }

    /**
     * Tree Data
     * @param Request $request
     * @return mixed
     */
    public function treeData (Request $request)
    {
        $menus = Menu::get()->toTree();
        if (!empty($roleId = $request->get('roleId'))) {
            $menus = Menu::menuTransformChecked($menus, RoleMenu::where('role_id', $roleId)->pluck('menu_id')->toArray());
            return ResponseUtil::success(MenuTreeResource::collection($menus));
        }
        return ResponseUtil::success(MenuTreeResource::collection($menus));
    }
}
