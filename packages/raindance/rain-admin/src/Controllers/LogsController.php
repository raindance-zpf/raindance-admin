<?php
/**
 * Created by PhpStorm.
 * User: raindance
 * Date: 2019/1/21
 * Time: 14:45
 */
namespace Raindance\RainAdmin\Controllers;

use Illuminate\Http\Request;
use Raindance\RainAdmin\Constants\CommonConst;
use Raindance\RainAdmin\Facades\RainAdmin;
use Raindance\RainAdmin\Models\AdminLog;
use Raindance\RainAdmin\Services\ResponseUtil;

class LogsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['web', 'admin', 'admin.role:super-admin']);
    }

    public function index ()
    {
        return view('admin::log.index');
    }

    /**
     * Get list with paginate
     * @param Request $request
     * @return mixed
     */
    public function all (Request $request)
    {
        $models = AdminLog::with('causer')
            ->when($method = $request->get('requestMethod'), function ($q) use ($method) {
                return $q->where('method', $method);
            })
            ->when($uid = $request->get('userId'), function ($q) use ($uid) {
                return $q->where('user_id', $uid);
            })
            ->when($path = $request->get('path'), function ($q) use ($path) {
                return $q->where('path', 'like', "%{$path}%");
            })
            ->paginate(RainAdmin::pageSize());
        return ResponseUtil::success($models);
    }

    /**
     * Destroy the specify resource
     * @param $id
     * @return mixed
     */
    public function destroy ($id)
    {
        try {
            $ids = explode(",", $id);
            AdminLog::destroy($ids);
            return ResponseUtil::success();
        } catch (\Exception $exception) {
            return ResponseUtil::failed(CommonConst::DELETE_FAILED, 200);
        }
    }
}