<?php
/**
 * Created by PhpStorm.
 * User: raindance
 * Date: 2019/1/21
 * Time: 14:45
 */
namespace Raindance\RainAdmin\Controllers;


class LogviewerController extends Controller
{
    public function __construct()
    {
        $this->middleware(['web', 'admin', 'admin.role:super-admin']);
    }

    public function index ()
    {
        return view('admin::log-viewer.index');
    }
}