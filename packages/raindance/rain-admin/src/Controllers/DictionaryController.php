<?php

namespace Raindance\RainAdmin\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Raindance\RainAdmin\Facades\RainAdmin;
use Raindance\RainAdmin\Models\Dictionary;
use Raindance\RainAdmin\Requests\DictionaryCreateRequest;
use Raindance\RainAdmin\Requests\DictionaryUpdateRequest;
use Raindance\RainAdmin\Services\ResponseUtil;

class DictionaryController extends Controller
{
    public function __construct()
    {
        $this->middleware([
            'web',
            'admin',
            'admin.role:super-admin'
            ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin::dict.index');
    }
    public function all (Request $request)
    {
        $list = Dictionary::whereIsRoot()
            ->when($dictName = $request->get('dictName'), function ($q) use ($dictName) {
                return $q->where('dict_name', 'like', "%{$dictName}%");
            })
            ->when($dictType = $request->get('dictType'), function ($q) use ($dictType) {
                return $q->where('dict_type', 'like', "%{$dictType}%");
            })
            ->orderBy('sort_order')
            ->paginate(RainAdmin::pageSize());
        return ResponseUtil::success($list);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin::dict.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  DictionaryCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DictionaryCreateRequest $request)
    {
        $data = $request->all();
        Dictionary::create($data);
        return ResponseUtil::success();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Dictionary::find($id);
        return ResponseUtil::success($model);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin::dict.edit')->withId($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  DictionaryUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DictionaryUpdateRequest $request, $id)
    {
        Dictionary::find($id)->update($request->all());
        return ResponseUtil::success();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $has = Dictionary::descendantsOf($id);
        if ($has->isNotEmpty()) {
            return ResponseUtil::failed('下面有字典数据，不可删除！请先删除子数据', 200);
        }
        Dictionary::destroy($id);
        return ResponseUtil::success();
    }
}
