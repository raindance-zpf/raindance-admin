<?php

namespace Raindance\RainAdmin\Controllers;

use Raindance\RainAdmin\Constants\CommonConst;
use Raindance\RainAdmin\Facades\RainAdmin;
use Raindance\RainAdmin\Models\Role;
use Raindance\RainAdmin\Services\ResponseUtil;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Exceptions\RoleAlreadyExists;

class RolesController extends Controller
{
    public function __construct()
    {
        $this->middleware(['web', 'admin', 'admin.role:super-admin']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('admin::role.index');
    }

    /**
     * Roles list with paginate
     * @return mixed
     */
    public function all (Request $request)
    {
        $models = Role::when($name = $request->get('name'), function ($q) use ($name) {
            return $q->where('name', 'like', "%{$name}%");
        })->when($slug = $request->get('slug'), function ($q) use ($slug) {
            return $q->where('slug', 'like', "%{$slug}%");
        })->latest()
            ->paginate(RainAdmin::pageSize());
        return ResponseUtil::success($models);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin::role.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $request->offsetSet('guard_name', 'admin');
            $data = $request->except(['permissions', 'menus']);
            $result = Role::create($data);
            Role::setSyncPermissionTo($result, $request->get('permissions'));
            Role::saveToMenu($result, $request->get('menus'));
            DB::commit();
            return ResponseUtil::success($result);
        } catch (RoleAlreadyExists $exception) {
            DB::rollBack();
            return ResponseUtil::failed('角色已经存在', 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return ResponseUtil::failed($e->getMessage());
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Role::findById($id, 'admin');
        $model->permissions;
        return ResponseUtil::success($model);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin::role.edit')->withId($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            if (Role::where('name', $request->name)->where('id', '<>', $id)->exists()) {
                return ResponseUtil::failed('角色已经存在.', 200);
            }
            $role = Role::findOrFail($id);
            $role->update($request->except(['permissions', 'menus']));
            Role::setSyncPermissionTo($role, $request->get('permissions'));
            Role::saveToMenu($role, $request->get('menus'));
            DB::commit();
            return ResponseUtil::success();
        } catch (ModelNotFoundException $e) {
            DB::rollBack();
            return ResponseUtil::notFound();
        } catch (\Exception $exception) {
            DB::rollBack();
            return ResponseUtil::failed(CommonConst::ALTER_FAILED, 200);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $role = Role::findById($id, 'admin');
            if ($role && $role->name === 'super-admin') {
                return ResponseUtil::failed('超级管理员角色不可删除', 200);
            }
            $role->permissions()->detach();
            $role->menus()->detach();
            $count = $role->delete();
            DB::commit();
            if($count > 0) {
                return ResponseUtil::success();
            }
            return ResponseUtil::failed(CommonConst::DELETE_FAILED, 200);
        } catch (\Exception $exception) {
            DB::rollBack();
            return ResponseUtil::failed(CommonConst::DELETE_FAILED, 200);
        }
    }
}
