<?php

namespace Raindance\RainAdmin\Controllers;

use Raindance\RainAdmin\Constants\CommonConst;
use Raindance\RainAdmin\Facades\RainAdmin;
use Raindance\RainAdmin\Services\ResponseUtil;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Spatie\Permission\Exceptions\PermissionAlreadyExists;
use Spatie\Permission\Models\Permission;

class PermissionsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['web', 'admin', 'admin.role:super-admin']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('admin::permission.index');
    }

    /**
     * Permission list with paginate
     * @return mixed
     */
    public function withPagination (Request $request)
    {
        $models = Permission::when($name = $request->get('name'), function ($q) use ($name) {
            return $q->where('name', 'like', "%{$name}%");
        })->when($slug = $request->get('slug'), function ($q) use ($slug) {
            return $q->where('slug', 'like', "%{$slug}%");
        })->latest()
            ->paginate(RainAdmin::pageSize());
        return ResponseUtil::success($models);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin::permission.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->offsetSet('guard_name', 'admin');
            $data = $request->all();
            $result = Permission::create($data);
            return ResponseUtil::success($result);
        } catch (PermissionAlreadyExists $exception) {
            return ResponseUtil::failed('权限已经存在.', 200);
        } catch (\Exception $e) {
            return ResponseUtil::failed(CommonConst::ADD_FAILED);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return ResponseUtil::success(Permission::findOrFail($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin::permission.edit')->withId($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            if (Permission::where('name', $request->name)->where('id', '<>', $id)->exists()) {
                return ResponseUtil::failed('权限已经存在.', 200);
            }
            Permission::findOrFail($id)->update($request->all());
            return ResponseUtil::success();
        } catch (ModelNotFoundException $e) {
            return ResponseUtil::notFound();
        } catch (\Exception $exception) {
            return ResponseUtil::failed($exception->getMessage(), 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $count = Permission::destroy($id);
        if($count > 0) {
            return ResponseUtil::success();
        }
        return ResponseUtil::faield(CommonConst::DELETE_FAILED, 200);
    }
}
