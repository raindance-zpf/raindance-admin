<?php
/**
 * Created by PhpStorm.
 * User: raindance
 * Date: 2019/1/21
 * Time: 14:45
 */
namespace Raindance\RainAdmin\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Raindance\RainAdmin\Constants\CommonConst;
use Raindance\RainAdmin\Services\ResponseUtil;
use Raindance\RainAdmin\Services\UploadService;

class UploadController extends Controller
{
    private $uploadService;

    public function __construct(UploadService $uploadService)
    {
        $this->uploadService = $uploadService;
    }

    /**
     * Upload Image
     * @param Request $request
     * @return mixed
     */
    public function uploadImage (Request $request)
    {
        $path = $this->uploadService
            ->uniqueName()
            ->upload($request->file('file'));
        return ResponseUtil::success($this->uploadService->storage->url($path));
    }

    /**
     * Remove the specify resource
     * @param Request $request
     * @return mixed
     */
    public function remove (Request $request)
    {
        try {

            $file = Str::after($request->get('filename'), $this->uploadService->storage->getConfig()->get('url'));
            $this->uploadService->destroy(ltrim($file, '/'));
            return ResponseUtil::success();
        } catch (\Exception $exception) {
            return ResponseUtil::failed(CommonConst::DELETE_FAILED, 200);
        }
    }
}