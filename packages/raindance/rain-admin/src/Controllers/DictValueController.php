<?php

namespace Raindance\RainAdmin\Controllers;

use Illuminate\Http\Request;
use Raindance\RainAdmin\Facades\RainAdmin;
use Raindance\RainAdmin\Models\Dictionary;
use Raindance\RainAdmin\Requests\DictValueCreateRequest;
use Raindance\RainAdmin\Services\ResponseUtil;

class DictValueController extends Controller
{
    public function __construct()
    {
        $this->middleware(['web', 'admin', 'admin.role:super-admin']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('admin::dict_value.index', ['pid' => $request->get('pid'), 'dictType' => $request->get('dictType')]);
    }
    public function all (Request $request)
    {
        $list = Dictionary::orderBy('sort_order')
            ->where('parent_id', $request->get('pid'))
            ->paginate(RainAdmin::pageSize());
        return ResponseUtil::success($list);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('admin::dict_value.create', ['pid' => $request->get('pid'), 'dictType' => $request->get('dictType')]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  DictValueCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DictValueCreateRequest $request)
    {
        $data = $request->all();
        Dictionary::create($data);
        return ResponseUtil::success();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Dictionary::find($id);
        return ResponseUtil::success($model);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin::dict_value.edit')->withId($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  DictValueCreateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DictValueCreateRequest $request, $id)
    {
        Dictionary::find($id)->update($request->all());
        return ResponseUtil::success();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Dictionary::destroy($id);
        return ResponseUtil::success();
    }
}
