<?php

namespace Raindance\RainAdmin\Controllers;


use Raindance\RainAdmin\Constants\CommonConst;
use Raindance\RainAdmin\Facades\RainAdmin;
use Raindance\RainAdmin\Models\AdminUser;
use Raindance\RainAdmin\Requests\UserCreateRequest;
use Raindance\RainAdmin\Requests\UserUpdateRequest;
use Raindance\RainAdmin\Services\ResponseUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class UsersController extends Controller
{

    public function __construct()
    {
        $this->middleware(['web', 'admin', 'admin.role:super-admin']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response|View
     */
    public function index(Request $request)
    {
        return view('admin::user.index');
    }

    /**
     * 用户列表
     * @return mixed
     */
    public function all (Request $request)
    {

        $models = AdminUser::when($userId = $request->get('userId'), function ($q) use ($userId) {
            return $q->where('id', $userId);
        })->when($username = $request->get('username'), function ($q) use ($username) {
            return $q->where('name', 'like', "%{$username}%");
        })->paginate(RainAdmin::pageSize());
        $models->each(function ($item) {
            $item->roles = $item->getRoleNames();
        });
        return ResponseUtil::success($models);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin::user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  UserCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserCreateRequest $request)
    {
        DB::beginTransaction();
        try {
            $result = AdminUser::createUser($request->all());
            AdminUser::attachRole($result, $request->get('roles'));
            AdminUser::attachPermission($result, $request->get('permissions'));
            DB::commit();
            return ResponseUtil::success($result);
        } catch (\Exception $exception) {
            DB::rollBack();
            return ResponseUtil::failed($exception->getMessage(), 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = AdminUser::find($id);
        $user->roles;
        $user->permissions;
        return ResponseUtil::success($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin::user.edit')->withId($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UserUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $result = AdminUser::findOrFail($id);
            AdminUser::updateUser($result, $request->except(['roles', 'permissions']));
            AdminUser::attachRole($result, $request->get('roles'));
            AdminUser::attachPermission($result, $request->get('permissions'));
            DB::commit();
            return ResponseUtil::success();
        } catch (\Exception $exception) {
            DB::rollBack();
            logger(sprintf(now()->toDateTimeString() . ":%s", $exception->getMessage()));
            return ResponseUtil::failed(CommonConst::ALTER_FAILED, 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (RainAdmin::isAdministrator($id)) {
            return ResponseUtil::failed('超级管理员无法删除', 200);
        }

        DB::beginTransaction();
        try {
            $user = AdminUser::findOrFail($id);
            $user->roles()->detach();
            $user->permissions()->detach();
            $user->delete();
            DB::commit();
            return ResponseUtil::success();
        } catch (\Exception $exception) {
            DB::rollBack();
            logger(sprintf(now()->toDateTimeString() . ":%s", $exception->getMessage()));
            return ResponseUtil::failed(CommonConst::DELETE_FAILED, 200);
        }
    }
}
