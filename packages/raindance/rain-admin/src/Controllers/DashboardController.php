<?php
/**
 * Created by PhpStorm.
 * User: raindance
 * Date: 2019/1/21
 * Time: 14:45
 */
namespace Raindance\RainAdmin\Controllers;


use Raindance\RainAdmin\Services\DictService;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware(['web', 'admin']);
    }

    public function index ()
    {
        //dd(DictService::getDictToArray('gender'));
        return view('admin::dashboard.index');
    }
}