<?php
/**
 * Created by PhpStorm.
 * User: raindance
 * Date: 2019/1/21
 * Time: 14:45
 */
namespace Raindance\RainAdmin\Controllers;

use Illuminate\Support\Facades\Auth;
use Raindance\RainAdmin\Constants\CommonConst;
use Raindance\RainAdmin\Facades\RainAdmin;
use Raindance\RainAdmin\Models\AdminUser;
use Raindance\RainAdmin\Requests\UserUpdateRequest;
use Raindance\RainAdmin\Services\ResponseUtil;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware(['web', 'admin']);
    }

    public function index ()
    {
        return view('admin::profile.index');
    }

    /**
     * Profile info
     * @return mixed
     */
    public function info ()
    {
        $user = Auth::guard('admin')->user();
        return ResponseUtil::success($user);
    }

    /**
     * Update profile
     * @param UserUpdateRequest $request
     * @return mixed
     */
    public function update (UserUpdateRequest $request)
    {
        try {
            $user = RainAdmin::user();
            AdminUser::updateUser($user, $request->all());
            return ResponseUtil::success();
        } catch (\Exception $exception) {
            return ResponseUtil::failed(CommonConst::OPERATION_FAILED, 200);
        }
    }
}