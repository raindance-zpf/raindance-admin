<?php
/**
 * Created by PhpStorm.
 * User: raindance
 * Date: 2019/6/11
 * Time: 17:22
 */

namespace Raindance\RainAdmin\Exceptions;

use Raindance\RainAdmin\Services\ResponseUtil;

class AuthorizationApiException extends \Illuminate\Auth\Access\AuthorizationException
{
    public function render ()
    {
        return ResponseUtil::failed('This action is unauthorized.', 200);
    }
}