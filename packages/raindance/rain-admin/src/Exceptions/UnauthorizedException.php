<?php
/**
 * Created by PhpStorm.
 * User: raindance
 * Date: 2019/3/14
 * Time: 16:27
 */
namespace Raindance\RainAdmin\Exceptions;

use Throwable;
use Spatie\Permission\Exceptions\UnauthorizedException as Unauthorized;

class UnauthorizedException extends Unauthorized
{
    public function __construct(int $code = 0, string $message, Throwable $previous = null)
    {
        parent::__construct( 0, "Unauthorized", $previous, []);
    }
    public function render ()
    {
        return response()->view('admin::errors.unauthorized');
    }
}