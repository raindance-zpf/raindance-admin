<?php
/**
 * Created by PhpStorm.
 * User: raindance
 * Date: 2019/1/22
 * Time: 11:00
 */

namespace Raindance\RainAdmin\Constants;


class CommonConst
{
    const ADD_SUCCESS = '添加成功';
    const ADD_FAILED = '添加失败';
    const CREATE_SUCCESS = '创建成功';
    const CREATE_FAILED = '创建失败';
    const OPERATION_SUCCESS = '操作成功';
    const OPERATION_FAILED = '操作失败';
    const DELETE_SUCCESS = '删除成功';
    const DELETE_FAILED = '删除失败';
    const ALTER_SUCCESS = '修改成功';
    const ALTER_FAILED = '修改失败';

    const ADMIN_ID = 1;
}