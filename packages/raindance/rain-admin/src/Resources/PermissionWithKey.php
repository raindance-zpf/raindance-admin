<?php

namespace Raindance\RainAdmin\Resources;

use Illuminate\Http\Resources\Json\Resource;

class PermissionWithKey extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'key' => $this->id,
            'label' => $this->slug,
            'name' => $this->name
        ];
    }
}
