<?php

namespace Raindance\RainAdmin\Resources;

use Illuminate\Http\Resources\Json\Resource;

class MenuTreeResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'checked' => $this->checked,
            'expand' => true,
            'children' => MenuTreeResource::collection($this->children)
        ];
    }
}
