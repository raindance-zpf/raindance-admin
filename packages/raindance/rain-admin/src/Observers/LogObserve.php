<?php
/**
 * Created by PhpStorm.
 * User: raindance
 * Date: 2019/3/13
 * Time: 12:04
 */
namespace Raindance\RainAdmin\Observers;

use Illuminate\Database\Eloquent\Model;
use Raindance\RainAdmin\Services\LogService;

class LogObserve
{
    /**
     * Deleted event
     * @param Model $model
     */
    public function deleted (Model $model)
    {
        if (config('admin.admin_logs.enable')) {

            try {
                $logger = new LogService();
                $logger->performedOn($model)
                    ->log("Delete the resource");
            } catch (\Exception $exception) {
                logger("Deleted event execute exception: {$exception->getMessage()}");
            }
        }
    }

    /**
     * Updated event
     * @param Model $model
     */
    public function updated (Model $model)
    {
        if (config('admin.admin_logs.enable')) {
            try {
                $logger = new LogService();
                $logger->performedOn($model)
                    ->log("Update the resource");
            } catch (\Exception $exception) {
                logger("Updated event execute exception: {$exception->getMessage()}");
            }
        }
    }

    /**
     * Created event
     * @param Model $model
     */
    public function created (Model $model)
    {
        if (config('admin.admin_logs.enable')) {
            try {
                $logger = new LogService();
                $logger->performedOn($model)
                    ->log("Create the resource");
            } catch (\Exception $exception) {
                logger("Created event execute exception: {$exception->getMessage()}");
            }
        }
    }
}