<?php

namespace Raindance\RainAdmin\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|unique:admin_users,name,'.$this->id.'|max:255',
            'password' => 'string|min:6|confirmed',
        ];
    }
    public function messages()
    {
        return [
            'required' => ':attribute 必填',
            'string' => ':attribute 必须是字符串',
            'max' => ':attribute 不能大于:max位',
            'min' => ':attribute 不能少于:min位',
            'confirmed' => ':attribute 不能与确认密码匹配',
            'unique' => ':attribute 已经存在'
        ];
    }
    public function attributes()
    {
        return [
            'name' => '用户名',
            'password' => '密码'
        ];
    }
}
