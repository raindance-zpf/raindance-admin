<?php

namespace Raindance\RainAdmin\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DictionaryUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dict_name' => 'required',
            'dict_type' => 'required|string',
        ];
    }
    public function messages()
    {
        return [
            'required' => ':attribute 必填',
            'string' => ':attribute 必须是字符串',
            'unique' => ':attribute 已经存在'
        ];
    }
    public function attributes()
    {
        return [
            'dict_name' => '字典名称',
            'dict_type' => '字典类型'
        ];
    }
}
