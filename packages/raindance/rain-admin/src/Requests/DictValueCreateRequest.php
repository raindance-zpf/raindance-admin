<?php

namespace Raindance\RainAdmin\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DictValueCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dict_label' => 'required',
            'dict_value' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'required' => ':attribute 必填'
        ];
    }
    public function attributes()
    {
        return [
            'dict_label' => '字典标签',
            'dict_value' => '字典值'
        ];
    }
}
