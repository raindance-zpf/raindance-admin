<?php
/**
 * Created by PhpStorm.
 * User: raindance
 * Date: 2019/3/7
 * Time: 17:07
 */
namespace Raindance\RainAdmin;


use Illuminate\Support\Facades\Gate;
use Raindance\RainAdmin\Middleware\AdminAuthenticate;
use Illuminate\Support\ServiceProvider;
use Raindance\RainAdmin\Middleware\AdminPermissionMiddleware;
use Raindance\RainAdmin\Middleware\AdminRoleMiddleware;
use Raindance\RainAdmin\Middleware\AdminRoleOrPermissionMiddleware;
use Raindance\RainAdmin\Services\QueryLoggerService;


class RainAdminServiceProvider extends ServiceProvider
{
    /**
     * The application's route middleware.
     * @var array
     *
     */
    protected $routeMiddleware = [
        'admin.auth' => AdminAuthenticate::class,
        'admin.role' => AdminRoleMiddleware::class,
        'admin.permission' => AdminPermissionMiddleware::class,
        'admin.role_or_permission' => AdminRoleOrPermissionMiddleware::class,
    ];
    /**
     * The application's route middleware groups.
     * @var array
     */
    protected $middlewareGroups = [
        'admin' => [
            'admin.auth'
        ]
    ];

    public function boot ()
    {
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'admin');   //Load views

        //Load routes
        if (file_exists($routes = __DIR__.'/routes/routes.php')) {
            $this->loadRoutesFrom($routes);
        }
        /*if (file_exists($breadcrumbs = __DIR__.'/routes/breadcrumbs.php')) {
            $this->loadRoutesFrom($breadcrumbs);
        }*/


        if ($this->app->runningInConsole()) {
            $this->publishes([__DIR__.'/../config/admin.php' => config_path('admin.php')], 'rain-admin-config');
            $this->publishes([__DIR__.'/../resources/lang' => resource_path('lang')], 'rain-admin-lang');
            $this->publishes([__DIR__.'/../resources/assets' => public_path('vendor/rain-admin')], 'rain-admin-assets');
            $this->publishes([__DIR__.'/../resources/views/errors' => resource_path('views/errors')], 'rain-admin-errors');
        }
        if ($this->app['config']->get('admin.query_logger_enable')) {
            QueryLoggerService::registerListener();
        }
    }


    public function register ()
    {
        $this->loadAdminAuthConfig();
        $this->registerRouteMiddleware();
        $this->app->singleton(RainAdmin::class, function () {
            return new RainAdmin();
        });
    }


    /**
     * Setup auth configuration.
     *
     * @return void
     */
    protected function loadAdminAuthConfig()
    {
        config(array_dot(config('admin.auth', []), 'auth.'));
    }


    /**
     * Register the route middleware.
     *
     * @return void
     */
    protected function registerRouteMiddleware()
    {
        // register route middleware.
        foreach ($this->routeMiddleware as $key => $middleware) {
            app('router')->aliasMiddleware($key, $middleware);
        }

        // register middleware group.
        foreach ($this->middlewareGroups as $key => $middleware) {
            app('router')->middlewareGroup($key, $middleware);
        }
    }
}