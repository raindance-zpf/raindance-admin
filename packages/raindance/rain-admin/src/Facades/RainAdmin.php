<?php

namespace Raindance\RainAdmin\Facades;

use Illuminate\Support\Facades\Facade;


class RainAdmin extends Facade
{
    protected static function getFacadeAccessor()
    {
        return \Raindance\RainAdmin\RainAdmin::class;
    }
}
