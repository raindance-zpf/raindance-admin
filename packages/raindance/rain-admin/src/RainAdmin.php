<?php
/**
 * Created by PhpStorm.
 * User: raindance
 * Date: 2019/3/7
 * Time: 17:12
 */
namespace Raindance\RainAdmin;

use Illuminate\Support\Facades\Auth;
use Raindance\RainAdmin\Models\Menu;
use Raindance\RainAdmin\Models\Permission;
use Raindance\RainAdmin\Models\Role;
use Raindance\RainAdmin\Services\UploadService;

class RainAdmin
{
    /**
     * Get current login user.
     * @return mixed
     */
    public function user ()
    {
        return Auth::guard('admin')->user();
    }

    /**
     * Get current login user ID
     * @return mixed
     */
    public function getUserId ()
    {
        return Auth::guard('admin')->id();
    }
    /**
     * Get current login avatar of user
     * @return mixed|string
     */
    public function avatar ()
    {
        return is_null($avatar = object_get($this->user(), 'avatar'))
            ? asset('vendor/rain-admin/AdminLTE/dist/img/user2-160x160.jpg')
            : $avatar;
    }

    /**
     * Get current login username
     * @return mixed
     */
    public function username ()
    {
        return object_get($this->user(), 'name');
    }

    /**
     * Menu tree set
     * @return mixed
     */
    public function menuTreeSet ()
    {
        return Menu::menuTreeSet();
    }

    /**
     * @param $menuId
     * @return bool
     */
    public function roleHasMenu ($menuId)
    {
        return Role::roleHasMenu($menuId);
    }

    /**
     * Determine if current user is administrator
     *
     * @return mixed
     */
    public function isAdministrator ($id = null)
    {
        return Permission::isAdministrator($id);
    }

    /**
     * Get page size form http/https
     * @return mixed
     */
    public function pageSize ()
    {
        return request()->get('pageSize', config('admin.page_size'));
    }
}
