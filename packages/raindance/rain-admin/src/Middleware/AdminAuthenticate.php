<?php
/**
 * Created by PhpStorm.
 * User: raindance
 * Date: 2019/3/7
 * Time: 18:02
 */
namespace Raindance\RainAdmin\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AdminAuthenticate
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard('admin')->guest()) {
            return redirect()->guest('admin/login');
        }
        return $next($request);
    }
}