<?php
namespace Raindance\RainAdmin\Middleware;

use Illuminate\Support\Facades\Auth;
use Raindance\RainAdmin\Exceptions\UnauthorizedException;
use Closure;

/**
 * Created by PhpStorm.
 * User: raindance
 * Date: 2019/6/10
 * Time: 17:25
 */

class AdminRoleMiddleware extends \Spatie\Permission\Middlewares\RoleMiddleware
{
    public function handle($request, Closure $next, $role)
    {
        if (Auth::guard('admin')->guest()) {
            throw UnauthorizedException::notLoggedIn();
        }

        $roles = is_array($role)
            ? $role
            : explode('|', $role);

        if (! Auth::guard('admin')->user()->hasAnyRole($roles)) {
            throw UnauthorizedException::forRoles($roles);
        }

        return $next($request);
    }
}