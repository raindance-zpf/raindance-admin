<?php
/**
 * Created by PhpStorm.
 * User: raindance
 * Date: 2019/6/10
 * Time: 17:40
 */

namespace Raindance\RainAdmin\Middleware;


use Closure;
use Raindance\RainAdmin\Exceptions\UnauthorizedException;
use Spatie\Permission\Middlewares\PermissionMiddleware;

class AdminPermissionMiddleware extends PermissionMiddleware
{
    public function handle($request, Closure $next, $permission)
    {
        if (app('auth')->guard('admin')->guest()) {
            throw UnauthorizedException::notLoggedIn();
        }

        $permissions = is_array($permission)
            ? $permission
            : explode('|', $permission);

        foreach ($permissions as $permission) {
            if (app('auth')->guard('admin')->user()->can($permission)) {
                return $next($request);
            }
        }

        throw UnauthorizedException::forPermissions($permissions);
    }
}