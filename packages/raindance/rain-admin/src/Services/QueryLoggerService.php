<?php
/**
 * Author: raindance<121681289@qq.com>
 * DateTime: 2019/9/4 11:18
 * Description:
 */

namespace Raindance\RainAdmin\Services;


use Illuminate\Database\Events\QueryExecuted;
use Illuminate\Support\Facades\DB;

class QueryLoggerService
{
    public function __construct()
    {
    }

    public static function registerListener ()
    {
        if (!app('config')->get('app.debug')) {
            return;
        }

        DB::listen(function (QueryExecuted $query) {
            $sqlWithPlaceholders = str_replace(['%', '?'], ['%%', '%s'], $query->sql);
            $bindings = $query->connection->prepareBindings($query->bindings);
            $pdo = $query->connection->getPdo();
            $realSql = vsprintf($sqlWithPlaceholders, array_map([$pdo, 'quote'], $bindings));
            $duration = static::formatDuration($query->time / 1000);
            logger(sprintf('[%s] %s | %s: %s', $duration, $realSql, request()->method(), request()->getRequestUri()));
        });
    }
    private static function formatDuration($seconds)
    {
        if ($seconds < 0.001) {
            return round($seconds * 1000000).'μs';
        } elseif ($seconds < 1) {
            return round($seconds * 1000, 2).'ms';
        }
        return round($seconds, 2).'s';
    }
}