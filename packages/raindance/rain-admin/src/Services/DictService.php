<?php
/**
 * Created by PhpStorm.
 * User: raindance
 * Date: 2019/3/22
 * Time: 10:25
 */

namespace Raindance\RainAdmin\Services;

use Illuminate\Support\Facades\Cache;
use Raindance\RainAdmin\Models\Dictionary;

class DictService
{
    public static function findByType ($type)
    {
        if (empty($type)) return '';

        return  Dictionary::whereIsRoot()
            ->where('dict_type', $type)
            ->where('status', 1)
            ->first();
    }

    public static function getDict ($type)
    {
        $value = Cache::rememberForever((new static())->getDictKey($type), function () use ($type) {
            if ($parent = static::findByType($type)) {
                return Dictionary::whereDescendantOf($parent)->get();
            }
            return '';
        });
        return $value;
    }
    public static function getDictToArray ($type)
    {
        if ($dict = static::getDict($type)) {
            return $dict->toArray();
        }
        return '';
    }
    protected function getDictKey ($type)
    {
        return sprintf("_begin_::%s::_end_", $type);
    }
}