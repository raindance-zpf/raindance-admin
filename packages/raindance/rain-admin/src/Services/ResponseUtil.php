<?php
/**
 * Created by PhpStorm.
 * User: raindance
 * Date: 2018/9/5
 * Time: 16:40
 */
namespace Raindance\RainAdmin\Services;

use BadMethodCallException;
use Symfony\Component\HttpFoundation\Response as FoundationResponse;


class ResponseUtil
{

    const CODE_SUCCESS = 0;
    const CODE_FAILED = 500;
    const FIELD_CODE = 'code';
    const FILED_MESSAGE = 'msg';
    /**
     * @var int
     */
    protected $statusCode = FoundationResponse::HTTP_OK;

    /**
     * @return mixed
     */
    protected function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param $statusCode
     * @return $this
     */
    protected function setStatusCode($statusCode)
    {

        $this->statusCode = $statusCode;
        return $this;
    }

    /**
     * @param $data
     * @param array $header
     * @return mixed
     */
    protected function respond($data, $header = [])
    {

        return Response()->json($data, $this->getStatusCode(), $header);
    }

    /**
     * @param int $statusCode
     * @param array $data
     * @param null $code
     * @return mixed
     */
    protected function status($code, array $data, $statusCode = null){

        if ($statusCode){
            $this->setStatusCode($statusCode);
        }
        $status = [
            static::FIELD_CODE => $code
        ];

        $data = array_merge($status, $data);
        return $this->respond($data);
    }

    /**
     * @param $message
     * @param int $statusCode
     * @param int $code
     * @return mixed
     */
    protected function failed($message = 'failed', $statusCode = FoundationResponse::HTTP_BAD_REQUEST, $code = self::CODE_FAILED){

        return $this->setStatusCode($statusCode)->message($message, $code);
    }

    /**
     * @param $errors
     * @param string $message
     * @param int $code
     * @return mixed
     */
    protected function invalidated ($errors, $message = 'invalidated',  $code = FoundationResponse::HTTP_UNPROCESSABLE_ENTITY)
    {
        return $this->status($code, [static::FILED_MESSAGE => $message, 'errors' => $errors], FoundationResponse::HTTP_UNPROCESSABLE_ENTITY);
    }
    /**
     * @param $message
     * @param int $code
     * @return mixed
     */
    protected function message($message, $code = self::CODE_SUCCESS){

        return $this->status($code, [self::FILED_MESSAGE => $message]);
    }

    /**
     * @param string $message
     * @return mixed
     */
    protected function error($message = "Internal Error!"){

        return $this->failed($message, FoundationResponse::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * @param int $code
     * @return mixed
     */
    protected function deleted ($code = self::CODE_SUCCESS)
    {
        return $this->status($code, [static::FILED_MESSAGE => 'deleted success'], FoundationResponse::HTTP_NO_CONTENT);
    }
    /**
     * @param string $message
     * @return mixed
     */
    protected function created($message = "created")
    {
        return $this->setStatusCode(FoundationResponse::HTTP_CREATED)
            ->message($message);

    }

    /**
     * @param $data
     * @param int $code
     * @return mixed
     */
    protected function success($data = null, $code = self::CODE_SUCCESS){

        return $this->status($code, compact('data'));
    }

    /**
     * @param string $message
     * @return mixed
     */
    protected function notFound($message = 'Not Found!')
    {
        return $this->failed($message,Foundationresponse::HTTP_NOT_FOUND);
    }

    public static function __callStatic ($method, $argument)
    {
        if (method_exists(new static(), $method)) {
            return (new static())->$method(...$argument);
        }
        throw new BadMethodCallException(
            "Method [$method] does not exist on class."
        );
    }

    public function __call($name, $arguments)
    {
        return $this->$name(...$arguments);
    }
}