<?php
/**
 * Author: raindance<121681289@qq.com>
 * DateTime: 2019/8/8 10:43
 * Description:
 */

namespace Raindance\RainAdmin\Services;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Request;
use Raindance\RainAdmin\Facades\RainAdmin;
use Raindance\RainAdmin\Models\AdminLog;

class LogService
{
    private $logModel;


    public function __construct()
    {
        $this->logModel = new AdminLog();
        $this->path();
        $this->setMethod();
        $this->setIp();
        $this->useLog();
        $this->causedBy();
        $this->withProperties([]);
    }

    /**
     * @param string $description
     * @return AdminLog
     */
    public function log(string $description)
    {

        $activity = $this->logModel;

        $activity->description = $description;

        $activity->save();

        $this->activity = null;

        return $activity;
    }

    public function setIp (string $ip = null)
    {
        if (is_null($ip)) {
            $this->getLogModel()->ip = Request::getClientIp();
            return $this;
        }
        $this->getLogModel()->ip = $ip;
        return $this;
    }
    public function path (string $path = null)
    {
        if (is_null($path)) {
            $this->getLogModel()->path = Request::path();
            return $this;
        }
        $this->getLogModel()->path = $path;
        return $this;
    }
    public function setMethod (string $method = null)
    {
        if (is_null($method)) {
            $this->getLogModel()->method = Request::method();
            return $this;
        }
        $this->getLogModel()->method = $method;
        return $this;
    }
    public function useLog (string $logName = null)
    {
        if (is_null($logName)) {
            $this->getLogModel()->log_name = config('admin.default_log_name');
            return $this;
        }
        $this->getLogModel()->log_name = $logName;
        return $this;
    }
    public function inLog(string $logName)
    {
        return $this->useLog($logName);
    }

    public function performedOn(Model $model)
    {
        $this->getLogModel()->subject()->associate($model);

        return $this;
    }

    /**
     * @param Model $model
     * @return LogService
     */
    public function on(Model $model)
    {
        return $this->performedOn($model);
    }

    /**
     * @param Model|null $model
     * @return $this
     */
    public function causedBy(Model $model = null)
    {
        if (is_null($model )) {
            $this->getLogModel()->causer()->associate(RainAdmin::user());
            return $this;
        }
        $this->getLogModel()->causer()->associate($model);

        return $this;
    }

    /**
     * @param $modelOrId
     * @return LogService
     */
    public function by($modelOrId)
    {
        return $this->causedBy($modelOrId);
    }
    /**
     * @return AdminLog
     */
    public function getLogModel(): AdminLog
    {
        return $this->logModel;
    }

    public function withProperties($properties)
    {
        $this->getLogModel()->properties = collect($properties);

        return $this;
    }

    public function withProperty(string $key, $value)
    {
        $this->getLogModel()->properties = $this->getLogModel()->properties->put($key, $value);

        return $this;
    }
    /**
     * @param AdminLog $logModel
     * @return $this
     */
    public function setLogModel(AdminLog $logModel)
    {
        $this->logModel = $logModel;
        return $this;
    }

}