(function (root, factory) {

    root['AdminTools'] = factory(this.jQuery)

}(this, function ($) {
    function AdminTools() {}

    AdminTools.prototype = {
        highlight: function (id) {
            const self = $('#menuShowId' + id);
            const lis = self.parents('li')
            lis.map(function () {
                const $this = $(this)
                $this.addClass('menu-open');
                $this.find('ul').show();
            })
        },
        axiosCatchError: function(obj) {
            if (obj instanceof Object) {
                let errMsg;
                const isError = Object.values(obj).some(function(v,k){
                    errMsg = v[0];
                    return errMsg !== null || errMsg !== undefined;
                });
                if (isError) {
                    return errMsg;
                }
                return null;
            }
            return null;
        },
        replaceUrl: function(str, replacement) {
            return str.replace('__param__', replacement);
        },
    }
    return new AdminTools()
}));