@extends('admin::layouts.master')

@section('title','用户管理')



@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <div class="margin-bottom">
                        <i-input style="width: 200px;" placeholder="用户ID" v-model="userId"></i-input>
                        <i-input style="width: 200px;"  placeholder="用户名" v-model="username"></i-input>
                        <i-button type="primary" @click="search" icon="ios-search">搜索</i-button>
                        <i-button type="info" @click="reset" icon="ios-refresh">重置</i-button>
                        <i-button type="success" to="{{route('users.create')}}" icon="ios-add-circle-outline">新增</i-button>

                    </div>
                </div>
                <div class="box-body">
                    <div class="table-content">
                        <i-table border :columns="columns" :data="data6"></i-table>
                        <page :total="total" :page-size="pageSize" @on-change="handlePaginate" show-total style="margin-top: 20px;"></page>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @stop
@section('javascript')
    <script>
        const defaultAvatar = "{{asset('vendor/rain-admin/AdminLTE/dist/img/user2-160x160.jpg')}}"
        var vm = new Vue({
            el: '#app',
            data: {
                userId: '',
                username: '',
                total: 10,
                pageSize: 15,
                currentPage: 1,
                columns: [
                    {
                        title: 'ID',
                        key: 'id',
                        width: 60,
                        align: 'center'

                    },
                    {
                        title: '用户名',
                        key: 'name',
                        width: 200,
                        align: 'center'

                    },
                    {
                        title: '头像',
                        key: 'avatar',
                        width: 160,
                        align: 'center',
                        render: (h, params) => {
                            return h('img', {
                                attrs: {
                                    src: params.row.avatar ? params.row.avatar : defaultAvatar
                                },
                                style: {
                                    height: '40px',
                                    width: '40px'
                                }
                            })
                        }

                    },
                    {
                        title: '角色',
                        key: 'roles',
                        width: 200,
                        align: 'center',
                        render: (h, params) => {
                            return h('span', {}, vm.showRole(params.row))
                        }

                    },
                    {
                        title: '创建时间',
                        key: 'created_at',
                        align: 'center',
                        width: 200
                    },
                    {
                        title: '操作',
                        key: 'action',
                        width: 150,
                        align: 'center',
                        render: (h, params) => {
                            return h('div', [
                                h('Button', {
                                    props: {
                                        type: 'primary',
                                        size: 'small'
                                    },
                                    style: {
                                        marginRight: '5px'
                                    },
                                    on: {

                                        click: () => {
                                            vm.show(params)
                                        }
                                    }
                                }, '修改'),
                                h('Button', {
                                    props: {
                                        type: 'error',
                                        size: 'small'
                                    },
                                    on: {
                                        click: () => {
                                            vm.remove(params)
                                        }
                                    }
                                }, '删除')
                            ]);
                        }
                    }
                ],
                data6: []

            },
            methods: {
                getData () {
                    const _this = this
                    axios.get("{{route('users.all')}}?page=" + this.currentPage + '&pageSize=' + this.pageSize + '&username=' + this.username + '&userId=' + this.userId).then(res => {
                        if (res.data.code === 0) {
                            if (res.data.data instanceof Object) {
                                _this.total = res.data.data.total
                                if(res.data.data.data instanceof Array) {
                                    _this.data6 = res.data.data.data
                                }

                            }
                        }
                    })
                },
                handlePaginate (num) {
                    this.currentPage = num
                    this.getData()
                },
                show (param) {
                    location.href = AdminTools.replaceUrl("{{route('users.edit', ['id' => '__param__'])}}", param.row.id)
                },
                remove (param) {
                    const _this = this
                    const id = param.row.id
                    this.$Modal.confirm({
                        title: '确定要删除吗？',
                        onOk: () => {
                            const url = AdminTools.replaceUrl("{{route('users.destroy', ['id' => '__param__'])}}", id)
                            axios.delete(url).then(res => {
                                if (res.data.code === 0) {
                                    _this.$Message.success('删除成功')
                                    setTimeout(function () {
                                        location.reload()
                                    }, 1500)
                                } else {
                                    _this.$Message.info(res.data.message);
                                }
                            })
                        },
                        onCancel: () => {
                            //this.$Message.info('Clicked cancel');
                        }
                    });
                },
                showRole (params) {
                    const roles = params.roles
                    let len;
                    if ((len = roles.length) > 0) {
                        let str = ''
                        roles.forEach((item, index) => {
                            str += item['slug'] + (index == len-1 ? '' : '|')
                        })
                        return str
                    } else {
                        return '--';
                    }
                },
                //检索
                search () {
                    this.getData()
                },
                //reset
                reset () {
                    this.userId = ''
                    this.username = ''
                    this.getData()
                }
            },
            created () {
                this.getData()
                this.$Message.config({
                    top:100,
                    duration: 2
                })

            }
        });

        AdminTools.highlight(1)
    </script>
    @stop