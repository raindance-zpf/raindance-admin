@extends('admin::layouts.master')

@section('title','用户管理')



@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">新增用户</h3>
                </div>
                <div class="box-body">
                    <div class="col-md-8">
                        <i-form :label-width="100" ref="formRel" :rules="ruleValidate" :model="formModel">
                            <div class="panel panel-bordered">
                                <div class="panel-body">
                                    <form-item label="用户名" prop="name">
                                        <i-input v-model="formModel.name" placeholder="输入用户名"></i-input>
                                    </form-item>
                                    <form-item label="密码" prop="password">
                                        <i-input v-model="formModel.password" type="password" placeholder="输入密码"></i-input>
                                    </form-item>
                                    <form-item label="确认密码" prop="password_confirmation">
                                        <i-input v-model="formModel.password_confirmation" type="password" placeholder="输入确认密码"></i-input>
                                    </form-item>
                                    <form-item label="角色">
                                        <i-select v-model="formModel.roles" multiple >
                                            <i-option v-for="item in roleSet" :value="item.id" :key="item.id">@{{ item.slug }}</i-option>
                                        </i-select>
                                    </form-item>
                                    <form-item label="权限">
                                        <Transfer
                                                :data="data1"
                                                filterable
                                                :target-keys="targetKeys"
                                                :render-format="render"
                                                :list-style="listStyle"
                                                @on-change="handleChange">
                                        </Transfer>
                                    </form-item>
                                    <form-item>
                                        <i-button type="primary" @click="handleSubmit('formRel')" :loading="loading">确定</i-button>
                                        <i-button type="default" @click="goBack()">返回</i-button>
                                    </form-item>
                                </div>
                            </div>
                        </i-form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @stop
@section('javascript')
    <script>
        var url = "{{route('users.store')}}"
        var home = "{{route('users.index')}}"
        var vm = new Vue({
            el: '#app',
            data() {
                const checkRepassword = function (rule, value, callback) {
                    if (vm.$data.formModel.password != '' && value == '') {
                        callback(new Error('确认密码不能为空'))
                    } else if (vm.$data.formModel.password != vm.$data.formModel.password_confirmation) {
                        callback(new Error('确认密码与密码不一致'))
                    } else {
                        callback();
                    }
                }
                return {
                    loading: false,
                    formModel: {
                        name: '',
                        password: '',
                        password_confirmation: '',
                        avatar: '',
                        roles: []
                    },
                    ruleValidate: {
                        name: [
                            {required: true, message: '必填', trigger: 'blur'}
                        ],
                        password: [
                            {required: true, message: '必填', trigger: 'blur'}
                        ],
                        password_confirmation: [
                            {required: false, validator: checkRepassword, trigger: 'blur'}
                        ]
                    },
                    data1: [],
                    targetKeys: [],
                    listStyle: {
                        width: '260px',
                        height: '500px'
                    },
                    roleSet: []
                }

            },
            methods: {
                goBack: function () {
                    history.back(-1)
                },
                handleSubmit (name) {
                    const _this = this
                    this.$refs[name].validate(function (valid) {
                        if (valid) {
                            _this.loading = true
                            vm.$data.formModel['permissions'] = vm.$data.targetKeys
                            axios.post(url, vm.$data.formModel).then(function (res) {
                                if (res.data.code === 0) {
                                    vm.$Message.success('操作成功.');
                                    setTimeout(function () {
                                        location.href = home
                                    }, 1500)
                                } else {
                                    _this.loading = false
                                    vm.$Message.error(res.data.message);
                                }
                            }).catch(err => {
                                _this.loading = false
                                let err1 = null;
                                if(err.response.status == 422) {
                                    const errors = err.response.data.errors
                                    Object.values(errors).forEach((v,k) => {
                                        err1 = v[0]
                                        return
                                    })
                                }
                                if (err1 != null) {
                                    vm.$Message.error(err1);
                                }
                            })
                        } else {
                            //vm.$Message.error('Fail!');
                        }
                    })
                },
                render (item) {
                    return item.label + ':' + item.name
                },
                handleChange (newTargetKeys) {
                    this.targetKeys = newTargetKeys;
                },
                getPermission() {
                    const _this = this
                    axios.get("{{route('permissions.all')}}").then(res => {
                        if (res.data.code === 0) {
                            if (res.data.data instanceof Object) {
                                _this.data1 = res.data.data
                            }
                        }
                    })
                },
                selectOptionsWithRole () {
                    const _this = this
                    axios.get("{{route('roles.selectOptions')}}").then(res => {
                        if (res.data.code === 0) {
                            _this.roleSet = res.data.data
                        }
                    })
                }
            },
            created () {
                this.getPermission()
                this.selectOptionsWithRole()
                this.$Message.config({
                    top:100,
                    duration: 2
                })
            }
        })
        AdminTools.highlight(1)

    </script>
    @endsection