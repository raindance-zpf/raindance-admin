@extends('admin::layouts.master')

@section('title','字典管理')


@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <div class="margin-bottom">
                        <i-input style="width: 200px;" placeholder="字典名称" v-model="dict_name"></i-input>
                        <i-input style="width: 200px;"  placeholder="字典类型" v-model="dict_type"></i-input>
                        <i-button type="primary" @click="search" icon="ios-search">搜索</i-button>
                        <i-button type="info" @click="reset" icon="ios-refresh">重置</i-button>
                        <i-button type="success" to="{{route('dict.create')}}" icon="ios-add-circle-outline">新增</i-button>

                    </div>

                </div>
                <div class="box-body">
                    <div class="table-content">
                        <i-table border :columns="columns" :data="data6"></i-table>
                        <page :total="total" :page-size="pageSize" @on-change="handlePaginate" show-total style="margin-top: 20px;"></page>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @stop
@section('javascript')
    <script>
        var vm = new Vue({
            el: '#app',
            data: {
                dict_name: '',
                dict_type: '',
                total: 10,
                pageSize: 15,
                currentPage: 1,
                columns: [
                    {
                        type: 'index',
                        title: '序号',
                        width: 70,
                        align: 'center'
                    },
                    {
                        title: '字典名称',
                        key: 'dict_name',
                        width: 260,
                        align: 'center'

                    },
                    {
                        title: '字典类型',
                        key: 'dict_type',
                        width: 200,
                        align: 'center',
                        render: (h, params) => {
                            return h('a', {
                                attrs: {
                                    href: "{{route('dictValue.index')}}?pid=" + params.row.id + '&dictType=' + params.row.dict_type
                                }
                            }, params.row.dict_type)
                        }

                    },
                    {
                        title: '描述',
                        key: 'remark',
                        width: 300,
                        align: 'center'
                    },
                    {
                        title: '状态',
                        key: 'status',
                        width: 100,
                        align: 'center',
                        render: (h, params) => {
                            return h('span', {
                                class: ['label ' + (params.row.status == true ? 'label-info' : 'label-warning')]
                            }, (params.row.status == true ? '正常' : '禁用'))
                        }
                    },
                    {
                        title: '创建时间',
                        key: 'created_at',
                        width: 200,
                        align: 'center'
                    },
                    {
                        title: '操作',
                        key: 'action',
                        width: 150,
                        align: 'center',
                        render: (h, params) => {
                            return h('div', [
                                h('Button', {
                                    props: {
                                        type: 'primary',
                                        size: 'small'
                                    },
                                    style: {
                                        marginRight: '5px'
                                    },
                                    on: {

                                        click: () => {
                                            vm.show(params)
                                        }
                                    }
                                }, '修改'),
                                h('Button', {
                                    props: {
                                        type: 'error',
                                        size: 'small'
                                    },
                                    on: {
                                        click: () => {
                                            vm.remove(params)
                                        }
                                    }
                                }, '删除')
                            ]);
                        }
                    }
                ],
                data6: []

            },
            methods: {
                getData () {
                    const _this = this
                    axios.get("{{route('dict.all')}}?page=" + this.currentPage + '&pageSize=' + this.pageSize + '&dictName=' + this.dict_name + '&dictType=' + this.dict_type).then(res => {
                        if (res.data.code === 0) {
                            if (res.data.data instanceof Object) {
                                _this.total = res.data.data.total
                                if(res.data.data.data instanceof Array) {
                                    _this.data6 = res.data.data.data
                                }

                            }
                        }
                    })
                },
                handlePaginate (num) {
                    this.currentPage = num
                    this.getData()
                },
                show (param) {
                    location.href = AdminTools.replaceUrl("{{route('dict.edit', ['id' => '__param__'])}}", param.row.id)
                },
                remove (param) {
                    const _this = this
                    this.$Modal.confirm({
                        title: '确定要删除吗？',
                        onOk: () => {
                            const url = AdminTools.replaceUrl("{{route('dict.destroy', ['id' => '__param__'])}}", param.row.id)
                            axios.delete(url).then(res => {
                                if (res.data.code === 0) {
                                    _this.$Message.success('删除成功')
                                    setTimeout(function () {
                                        location.reload()
                                    }, 1500)
                                } else {
                                    _this.$Message.info(res.data.message);
                                }
                            })
                        },
                        onCancel: () => {
                            //this.$Message.info('Clicked cancel');
                        }
                    });
                },
                //检索
                search () {
                    this.getData()
                },
                //reset
                reset () {
                    this.dict_name = ''
                    this.dict_type = ''
                    this.getData()
                }
            },
            created () {
                this.$Message.config({
                    top:100,
                    duration: 2
                })
                this.getData()
            }
        });
        AdminTools.highlight(1)
    </script>
    @stop