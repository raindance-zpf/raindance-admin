@extends('admin::layouts.master')

@section('title','字典管理')


@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <a class="btn btn-success" href="{{route('dictValue.create',['pid' => $pid, 'dictType' => $dictType])}}">新增</a>
                    <a class="btn btn-default" href="{{route('dict.index')}}">返回</a>
                </div>
                <div class="box-body">
                    <div class="table-content">
                        <i-table border :columns="columns" :data="data6"></i-table>
                        <page :total="total" :page-size="pageSize" @on-change="handlePaginate" show-total style="margin-top: 20px;"></page>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @stop
@section('javascript')
    <script>
        const pid = "{{$pid}}"
        const dictType = "{{$dictType}}"
        var vm = new Vue({
            el: '#app',
            data: {
                total: 10,
                pageSize: 15,
                currentPage: 1,
                columns: [
                    {
                        type: 'index',
                        title: '序号',
                        width: 70,
                        align: 'center'
                    },
                    {
                        title: '字典标签',
                        key: 'dict_label',
                        width: 260,
                        align: 'center'

                    },
                    {
                        title: '字典值',
                        key: 'dict_value',
                        width: 200,
                        align: 'center'

                    },
                    {
                        title: '描述',
                        key: 'remark',
                        width: 300,
                        align: 'center'
                    },
                    {
                        title: '状态',
                        key: 'status',
                        width: 100,
                        align: 'center',
                        render: (h, params) => {
                            return h('span', {
                                class: ['label ' + (params.row.status == true ? 'label-info' : 'label-warning')]
                            }, (params.row.status == true ? '正常' : '禁用'))
                        }
                    },
                    {
                        title: '创建时间',
                        key: 'created_at',
                        width: 200,
                        align: 'center'
                    },
                    {
                        title: '操作',
                        key: 'action',
                        width: 150,
                        align: 'center',
                        render: (h, params) => {
                            return h('div', [
                                h('Button', {
                                    props: {
                                        type: 'primary',
                                        size: 'small'
                                    },
                                    style: {
                                        marginRight: '5px'
                                    },
                                    on: {

                                        click: () => {
                                            vm.show(params)
                                        }
                                    }
                                }, '修改'),
                                h('Button', {
                                    props: {
                                        type: 'error',
                                        size: 'small'
                                    },
                                    on: {
                                        click: () => {
                                            vm.remove(params)
                                        }
                                    }
                                }, '删除')
                            ]);
                        }
                    }
                ],
                data6: []

            },
            methods: {
                getData () {
                    const _this = this
                    axios.get("{{route('dictValue.all')}}?pid=" +pid + '&page=' + this.currentPage + '&pageSize=' + this.pageSize).then(res => {
                        if (res.data.code === 0) {
                            if (res.data.data instanceof Object) {
                                _this.total = res.data.data.total
                                if(res.data.data.data instanceof Array) {
                                    _this.data6 = res.data.data.data
                                }

                            }
                        }
                    })
                },
                handlePaginate (num) {
                    this.currentPage = num
                    this.getData()
                },
                show (param) {
                    location.href = AdminTools.replaceUrl("{{route('dictValue.edit', ['id' => '__param__'])}}", param.row.id)
                },
                remove (param) {
                    const _this = this

                    this.$Modal.confirm({
                        title: '确定要删除吗？',
                        onOk: () => {
                            const url = AdminTools.replaceUrl("{{route('dictValue.destroy', ['id' => '__param__'])}}", param.row.id)
                            axios.delete(url).then(res => {
                                if (res.data.code === 0) {
                                    _this.$Message.success('删除成功')
                                    setTimeout(function () {
                                        location.reload()
                                    }, 1500)
                                } else {
                                    _this.$Message.info(res.data.message);
                                }
                            })
                        },
                        onCancel: () => {
                            //this.$Message.info('Clicked cancel');
                        }
                    });
                }
            },
            created () {
                this.$Message.config({
                    top:100,
                    duration: 2
                })
                this.getData()
            }
        });
        AdminTools.highlight(1)
    </script>
    @stop