@extends('admin::layouts.master')

@section('title','字典管理')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">新增数据</h3>
                </div>
                <div class="box-body">
                    <i-form :label-width="100" ref="formRel" :rules="ruleValidate" :model="formModel">
                        <div class="panel panel-bordered">
                            <div class="panel-body">
                                <form-item label="字典标签" prop="dict_label">
                                    <i-input v-model="formModel.dict_label" placeholder="输入字典名称"></i-input>
                                </form-item>
                                <form-item label="字典值" prop="dict_value">
                                    <i-input v-model="formModel.dict_value" placeholder="输入字典类型"></i-input>
                                </form-item>
                                <form-item label="备注">
                                    <i-input v-model="formModel.remark"></i-input>
                                </form-item>
                                <form-item label="排序">
                                    <i-input v-model="formModel.sort_order" placeholder="数字越小越靠前"></i-input>
                                </form-item>
                                <form-item label="状态">
                                    <i-switch size="large" v-model="formModel.status" @on-change="stateChange">
                                        <span slot="open">开启</span>
                                        <span slot="close">禁用</span>
                                    </i-switch>
                                </form-item>
                                <form-item>
                                    <i-button type="primary" @click="handleSubmit('formRel')" :loading="loading">确定</i-button>
                                    <i-button type="default" @click="goBack()">返回</i-button>
                                </form-item>
                            </div>
                        </div>
                    </i-form>
                </div>
            </div>
        </div>
    </div>
    @stop
@section('javascript')
    <script>
        const pid = "{{$pid}}"
        const dictType = "{{$dictType}}"
        var url = "{{route('dictValue.store')}}"

        var vm = new Vue({
            el: '#app',
            data: {
                loading: false,
                formModel: {
                    dict_label: '',
                    dict_value: '',
                    remark: '',
                    sort_order: '0',
                    status: true,
                    parent_id: pid,
                    dict_type: dictType
                },
                ruleValidate: {
                    dict_label: [
                        {required: true, message: '必填', trigger: 'blur'}
                    ],
                    dict_value: [
                        {required: true, message: '必填', trigger: 'blur'}
                    ]
                }
            },
            methods: {
                goBack: function () {
                    history.back(-1)
                },
                //状态开关
                stateChange (state) {
                    this.formModel.status = state
                },
                handleSubmit (name) {
                    const _this = this
                    this.$refs[name].validate(function (valid) {
                        if (valid) {
                            _this.loading = true
                            axios.post(url, vm.$data.formModel).then(function (res) {
                                if (res.data.code === 0) {
                                    vm.$Message.success('操作成功.');
                                    setTimeout(function () {
                                        location.href = '/admin/dictValue?pid=' + pid + '&dictType=' + dictType
                                    }, 1500)
                                } else {
                                    _this.loading = false
                                    vm.$Message.error(res.data.message);
                                }
                            }).catch(err => {
                                _this.loading = false
                                let err1 = null;
                                if(err.response.status == 422) {
                                    const errors = err.response.data.errors
                                    Object.values(errors).forEach((v,k) => {
                                        err1 = v[0]
                                        return
                                    })
                                }
                                if (err1 != null) {
                                    vm.$Message.error(err1);
                                }
                            })
                        } else {
                            //vm.$Message.error('Fail!');
                        }
                    })
                }
            },
            created () {
                this.$Message.config({
                    top:100,
                    duration: 2
                })
            }
        })
        AdminTools.highlight(1)
    </script>
    @endsection