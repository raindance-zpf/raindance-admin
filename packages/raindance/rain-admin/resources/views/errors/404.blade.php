@extends('admin::errors.layout')

@section('title','404找不到')


@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="error-page">
            <h2 class="headline text-yellow"> 404</h2>

            <div class="error-content">
                <h3 class="margin-bottom"><i class="fa fa-warning text-yellow"></i> Oops! Page not found.</h3>

                <p>
                    找不到您要查找的页面。
                    您可以 <a href="{{route('admin.dashboard')}}">返回Dashboard</a>。
                </p>
            </div>
            <!-- /.error-content -->
        </div>
        <!-- /.error-page -->
    </section>
    <!-- /.content -->
    @stop