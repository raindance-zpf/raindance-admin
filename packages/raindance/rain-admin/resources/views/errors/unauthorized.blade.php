@extends('admin::layouts.master')

@section('title','404找不到')


@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="error-page">
            <h2 class="headline text-yellow"> 401</h2>

            <div class="error-content">
                <h3 class="margin-bottom"><i class="fa fa-warning text-yellow"></i> Oops! 抱歉。</h3>

                <p>
                    <h5>您没有权限查看。</h5>
                </p>
            </div>
            <!-- /.error-content -->
        </div>
        <!-- /.error-page -->
    </section>
    <!-- /.content -->
    @stop