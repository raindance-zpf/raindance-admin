@extends('admin::errors.layout')

@section('title','500服务器错误')


@section('content')
    <!-- Main content -->
    <section class="content">

        <div class="error-page">
            <h2 class="headline text-red">500</h2>

            <div class="error-content">
                <h3 class="margin-bottom"><i class="fa fa-warning text-red"></i> Oops! Something went wrong.</h3>

                <p>
                    我们马上就修好。
                    您可以 <a href="{{route('admin.dashboard')}}">返回Dashboard</a> 。
                </p>
            </div>
        </div>
        <!-- /.error-page -->

    </section>
    <!-- /.content -->
    @stop