@extends('admin::layouts.master')

@section('title','角色管理')



@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">修改角色</h3>
                </div>
                <div class="box-body">
                    <i-form :label-width="100" ref="formRel" :rules="ruleValidate" :model="formModel">
                        <div class="panel panel-bordered">
                            <div class="panel-body">
                                <form-item label="名称" prop="slug">
                                    <i-input v-model="formModel.slug" placeholder="输入名称"></i-input>
                                </form-item>
                                <form-item label="标识" prop="name">
                                    <i-input v-model="formModel.name" placeholder="输入标识，必填是英文字母"></i-input>
                                </form-item>
                                {{--<form-item label="Guard Name" prop="guard_name">
                                    <i-input v-model="formModel.guard_name" placeholder="Guard Name example 'web'"></i-input>
                                </form-item>--}}
                                <form-item label="权限">
                                    <Transfer
                                            :data="data1"
                                            filterable
                                            :target-keys="targetKeys"
                                            :render-format="render"
                                            :list-style="listStyle"
                                            @on-change="handleChange">
                                    </Transfer>
                                </form-item>
                                <form-item label="菜单">
                                    <Tree :data="data2" @on-check-change="handleChangeTree" show-checkbox ref="tree"></Tree>
                                </form-item>
                                <form-item>
                                    <i-button type="primary" @click="handleSubmit('formRel')" :loading="loading">确定</i-button>
                                    <i-button type="default" @click="goBack()">返回</i-button>
                                </form-item>
                            </div>
                        </div>
                    </i-form>
                </div>
            </div>
        </div>
    </div>
    @stop
@section('javascript')
    <script>
        var url = "{{route('roles.update', ['id' => $id])}}"
        var home = "{{route('roles.index')}}"
        var dataId = "{{$id}}"
        var vm = new Vue({
            el: '#app',
            data: {
                loading: false,
                tmpMenus: [],
                menus: [],
                formModel: {
                    id: '',
                    name: '',
                    slug: '',
                    guard_name: 'admin'
                },
                ruleValidate: {
                    name: [
                        {required: true, message: '必填', trigger: 'blur'}
                    ],
                    slug: [
                        {required: true, message: '必填', trigger: 'blur'}
                    ]
                },
                data1: [],
                targetKeys: [],
                listStyle: {
                    width: '260px',
                    height: '500px'
                },
                data2: []
            },
            methods: {
                goBack: function () {
                    history.back(-1)
                },
                handleSubmit (name) {
                    const _this = this
                    this.$refs[name].validate(function (valid) {
                        if (valid) {
                            _this.loading = true
                            vm.$data.formModel['permissions'] = vm.$data.targetKeys
                            vm.$data.formModel['menus'] = vm.$data.menus
                            axios.put(url, vm.$data.formModel).then(function (res) {
                                if (res.data.code === 0) {
                                    vm.$Message.success('操作成功.');
                                    setTimeout(function () {
                                        location.href = home
                                    }, 1500)
                                } else {
                                    _this.loading = false
                                    vm.$Message.error(res.data.message);
                                }
                            })
                        } else {
                            vm.$Message.error('Fail!');
                        }
                    })
                },
                render (item) {
                    return item.label + ':' + item.name
                },
                handleChange (newTargetKeys) {
                    this.targetKeys = newTargetKeys;
                },
                getPermission() {
                    const _this = this
                    axios.get("{{route('permissions.all')}}").then(res => {
                        if (res.data.code === 0) {
                            if (res.data.data instanceof Object) {
                                _this.data1 = res.data.data
                            }
                        }
                    })
                },
                getRoles () {
                    const _this = this
                    const url = AdminTools.replaceUrl("{{route('roles.show', ['id' => '__param__'])}}", dataId)
                    axios.get(url).then(res => {
                        if (res.data.code === 0) {
                            if (typeof res.data.data == 'object') {
                                _this.formModel = res.data.data
                                if (res.data.data.permissions instanceof Array) {
                                    var perm = res.data.data.permissions
                                    perm.forEach(function ($item) {
                                        _this.targetKeys.push($item['id'])
                                    })
                                }
                            }
                        }
                    })
                },
                //Click checkout
                handleChangeTree () {
                    const tree = this.$refs.tree.getCheckedNodes()
                    const _this = this
                    this.menus = []
                    if (tree.length > 0) {
                        tree.forEach(function (value, key) {
                            _this.menus.push(value.id)
                        })
                    }
                },
                getTreeData () {
                    axios.get("{{route('menus.treeData')}}?roleId=" + dataId).then(res => {
                        if (res.data.code === 0) {

                            if (res.data.data) {
                                const d = res.data.data
                                this.data2 = d
                                this.menus = this.menusIds(d)

                            }
                        }
                    })
                },
                menusIds (d) {
                    const _this = this
                    d.forEach(function (v) {
                        if (v['checked'] == true) {
                            _this.tmpMenus.push(v['id'])
                        }
                        if (v['children'] && v['children'].length > 0) {
                            _this.menusIds(v['children'])
                        }
                    })
                    return _this.tmpMenus;
                }
            },
            created () {
                this.getPermission()
                this.getRoles()
                this.getTreeData()
                this.$Message.config({
                    top:100,
                    duration: 2
                })

            }
        })
        AdminTools.highlight(1)
    </script>
    @endsection