@extends('admin::layouts.master')

@section('title','log-viewer')


@section('content')
    <div class="row">
        <iframe src="{{route('log-viewer::dashboard')}}" width="100%" height="800"></iframe>
    </div>
    @stop
@section('javascript')
    <script>
        AdminTools.highlight(1)
        const vm = new Vue({
            el: '#app'
        })
    </script>
    @stop