<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        @if(config('admin.show_environment'))
            <strong>Env</strong>&nbsp;&nbsp; {!! env('APP_ENV') !!}
        @endif

            @if(config('admin.show_version'))
                <strong>Version</strong>&nbsp;&nbsp;{{config('admin.version')}}
            @endif
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2018-{{date('Y')}} <a href="http://www.raindance.top" target="_blank">Raindance</a>.</strong> All rights reserved.
</footer>