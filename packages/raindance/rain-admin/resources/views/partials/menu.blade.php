
    @foreach($items as $item)

        @php
            $hasChildren = false;
            if ($item->children->isNotEmpty()) {
                foreach($item->children as $child) {
                    if (RainAdmin::isAdministrator()) {
                        $hasChildren = true;
                    } else {
                        $hasChildren = $hasChildren || RainAdmin::roleHasMenu($child->id);
                    }
                }
                if (!$hasChildren) {
                    continue ;
                }
            } else {
                if (!RainAdmin::isAdministrator()) {
                    if (!RainAdmin::roleHasMenu($item->id)) {
                        continue;
                    }
                }
            }
        @endphp

        @if(!$hasChildren)
            <li class="@if(url()->current() == url($item->url)) active @endif">
                @if(url()->isValidUrl(url($item->url)))
                    <a href="{{ url($item->url) }}" >
                        <i class="fa {{$item->icon_class}}"></i>
                        <span>{{ $item->title }}</span>
                    </a>
                @else
                    <a href="javascript:void(0)">
                        <i class="fa {{$item->icon_class}}"></i>
                        <span>{{ $item->title }}</span>
                    </a>
                @endif
            </li>
        @else
            <li class="treeview " data-id="{{$item->id}}" id="menuOpenId{{$item->id}}">
                <a href="javascript:void(0)">
                    <i class="fa {{ $item->icon_class }}"></i>
                    <span>{{ $item->title }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu" data-id="{{$item->id}}" id="menuShowId{{$item->id}}">
                    @include('admin::partials.menu', ['items' => $item->children])
                </ul>

            </li>
        @endif

    @endforeach

{{--
@if(RainAdmin::isAdministrator() || RainAdmin::roleHasMenu($item->id))
    @if($item->children->isEmpty())
        <li class="@if(url()->current() == url($item->url)) active @endif">
            @if(url()->isValidUrl(url($item->url)))
                <a href="{{ url($item->url) }}" >
                    <i class="fa {{$item->icon_class}}"></i>
                    <span>{{ $item->title }}</span>
                </a>
                    @else
                        <a href="javascript:void(0)">
                            <i class="fa {{$item->icon_class}}"></i>
                            <span>{{ $item->title }}</span>
                        </a>
            @endif
        </li>
    @else
        <li class="treeview " data-id="{{$item->id}}" id="menuOpenId{{$item->id}}">
            <a href="javascript:void(0)">
                <i class="fa {{ $item->icon_class }}"></i>
                <span>{{ $item->title }}</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu" data-id="{{$item->id}}" id="menuShowId{{$item->id}}">
                @foreach($item['children'] as $item)
                    @include('admin::partials.menu', $item)
                @endforeach
            </ul>
        </li>
    @endif
@endif--}}
