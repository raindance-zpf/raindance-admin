<li class="dd-item" data-id="{{$treeData->id}}">
    <div class="dd-handle" >
        (ID:{{$treeData->id}}){{$treeData->title}}
        <span class="pull-right dd-nodrag">
            <a href="javascript:void(0)" class="label label-primary editMenu" data-id="{{$treeData->id}}">编辑</a>
            <a href="javascript:void(0);" data-id="{{ $treeData->id }}" class="label label-danger removeMenu">删除</a>
        </span>
    </div>

    @if(count($treeData['children']))
        <ol class="dd-list">
            @foreach($treeData->children as $treeData)
                @include('admin::menu.tree', $treeData)
            @endforeach
        </ol>

    @endif
</li>