@extends('admin::layouts.master')

@section('title','菜单管理')

@section('css')
    <link rel="stylesheet" href="{{asset('vendor/rain-admin/AdminLTE/plugins/nestable/nestable.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/rain-admin/AdminLTE/plugins/fontawesome-iconpicker/dist/css/fontawesome-iconpicker.min.css')}}">
    @stop

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">菜单管理</h3>
                </div>
                <div class="box-body">
                    <div class="col-md-6">
                        <div class="dd">
                            <ol class="dd-list">
                                @foreach($treeData as $treeData)
                                    @include('admin::menu.tree', $treeData)
                                @endforeach
                            </ol>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <i-form :label-width="200" ref="formRel" :rules="ruleValidate" :model="formModel">
                            <div class="panel panel-bordered">
                                <div class="panel-body">
                                    <form-item label="上级菜单">
                                        <i-select v-model="formModel.parent_id">
                                            <i-option v-for="item in menus" :value="item.id" :key="item.id" ><span v-html="item.title"></span></i-option>
                                        </i-select>
                                    </form-item>
                                    <form-item label="标题" prop="title">
                                        <i-input v-model="formModel.title" placeholder="输入标题"></i-input>
                                    </form-item>
                                   {{-- <form-item label="链接类型" >
                                        <i-select v-model="type" @on-change="handleTypeChange">
                                            <i-option v-for="item in links" :value="item.value" :key="item.value">@{{ item.label }}</i-option>
                                        </i-select>
                                    </form-item>--}}
                                    <form-item label="菜单URL">
                                        <i-input v-model="formModel.url" placeholder="菜单URL" ></i-input>
                                    </form-item>
                                    {{--<form-item label="菜单路由" v-show="!showUrl">
                                        <i-input v-model="formModel.route" placeholder="菜单路由" ></i-input>
                                    </form-item>--}}
                                    {{--<form-item label="路由参数（如果存在）" v-show="!showUrl">
                                        <i-input type="textarea" :rows="4" v-model="formModel.parameters" ></i-input>
                                    </form-item>--}}
                                    <form-item label="字体图标">
                                        <i-input ref="iconPicker" v-model="formModel.icon_class" placeholder="Icon Class （可选）" element-id="iconPicker"></i-input>
                                    </form-item>
                                    {{--<form-item label="打开方式">
                                        <i-select v-model="formModel.target" >
                                            <i-option v-for="item in targets" :value="item.value" :key="item.value">@{{ item.label }}</i-option>
                                        </i-select>
                                    </form-item>--}}
                                    <form-item label="排序" prop="sort_order" style="margin-bottom: 50px;">
                                        <i-input v-model="formModel.sort_order" placeholder="数字越小越靠前" number></i-input>
                                    </form-item>
                                    <form-item>
                                        <i-button type="primary" @click="handleSubmit('formRel')" :loading="loading">确定</i-button>
                                        <i-button type="default" @click="handleReset('formRel')">重置</i-button>
                                    </form-item>
                                </div>
                            </div>
                        </i-form>
                    </div>
                </div>
            </div>
        </div>

    </div>

    @stop
@section('javascript')
    <script src="{{asset('vendor/rain-admin/AdminLTE/plugins/nestable/jquery.nestable.js')}}"></script>
    <script src="{{asset('vendor/rain-admin/AdminLTE/plugins/fontawesome-iconpicker/dist/js/fontawesome-iconpicker.min.js')}}"></script>
    <script>
        var vm =  new Vue({
            el: '#app',
            data () {
                return {
                    loading: false,
                    formModel: {
                        title: '',
                        url: '',
                        route: '',
                        icon_class: '',
                        sort_order: 0,
                        parent_id: 0,
                        target: '_self'
                    },
                    editing: false,
                    showUrl: false,
                    type: 'route',
                    links: [
                        {
                            value: 'url',
                            label: '静态URL'
                        },
                        {
                            value: 'route',
                            label: '动态路由'
                        }
                    ],
                    targets: [
                        {
                            value: '_self',
                            label: '在相同标签/窗口打开'
                        },
                        {
                            value: '_blank',
                            label: '新标签页/窗口打开'
                        }
                    ],
                    menus: [
                        {
                            id: '0',
                            title: 'Root'
                        },
                    ],
                    ruleValidate: {
                        title: [
                            {required: true, message: '输入标题', trigger: 'blur'}
                        ],
                        sort_order: [
                            {type: 'number', message: '必须是数字', trigger: 'blur'}
                        ]
                    }
                }
            },
            methods: {
                handleTypeChange () {
                    if (this.type =='route') {
                        this.showUrl = false
                    } else {
                        this.showUrl = true
                    }
                },
                handleReset (name) {
                    this.$refs[name].resetFields()
                    this.editing = false
                },
                handleSubmit (name) {
                    const _this = this
                    this.$refs[name].validate(function (valid) {
                        if (valid) {
                            _this.loading = true
                            if (vm.$data.editing) {
                                const updateUrl = AdminTools.replaceUrl("{{route('menus.update', ['id' => '__param__'])}}", vm.$data.formModel.id)
                                axios.put(updateUrl, _this.formModel).then(function (res) {
                                    if (res.data.code === 0) {
                                        vm.$Message.success('操作成功.');
                                        setTimeout(function () {
                                            location.reload()
                                        }, 1500)
                                    } else {
                                        _this.loading = false
                                        vm.$Message.error(res.data.message);
                                    }
                                })
                            } else {
                                axios.post('{{route("menus.store")}}', vm.$data.formModel).then(function (res) {
                                    if (res.data.code === 0) {
                                        vm.$Message.success('操作成功.');
                                        setTimeout(function () {
                                            location.reload()
                                        }, 1500)
                                    } else {
                                        _this.loading = false
                                        vm.$Message.error(res.data.message);
                                    }
                                })
                            }
                        } else {
                            //vm.$Message.error('Fail!');
                        }
                    })
                },
                getSelectOptions () {
                    const _this = this
                    axios.get("{{route('menus.selectOptions')}}").then(res => {
                        if (res.data.code === 0) {
                            _this.menus = res.data.data
                        }
                    })
                }
            },
            created () {
                this.getSelectOptions()
                this.$Message.config({
                    top:100,
                    duration: 2
                })
            }
        });
        $('.dd').nestable({ /* config options */ });

        $('.editMenu').on('click', function () {
            const $this = $(this)
            const id = $this.data('id')
            const url = AdminTools.replaceUrl("{{route('menus.edit', ['id' => '__param__'])}}", id)
            axios.get(url).then(res => {
                if (res.data.code === 0) {
                    vm.$data.formModel = res.data.data
                    vm.$data.editing = true
                }
            })
        })
        $('.removeMenu').on('click', function () {
            const $this = $(this)
            const id = $this.data('id')
            vm.$Modal.confirm({
                title: '确定要删除吗？',
                onOk: () => {
                    const url = AdminTools.replaceUrl("{{route('menus.destroy', ['id' => '__param__'])}}", id)
                    axios.delete(url).then(res => {
                        if (res.data.code === 0) {
                            vm.$Message.info('删除成功')
                            setTimeout(function (){
                                location.reload()
                            }, 1500)
                        }
                    })
                },
                onCancel: () => {
                    //vm.$Message.info('Clicked cancel');
                }
            });
        })
        $('#iconPicker').iconpicker({
            placement:'bottomLeft',
            fullClassFormatter: function(val) {
                return 'fa ' + val;
            }
        })
        $('#iconPicker').on('iconpickerSelected', function(event){
            /* event.iconpickerValue */
            vm.$data.formModel.icon_class = event.iconpickerValue
        });
        AdminTools.highlight(1)
    </script>
    @stop