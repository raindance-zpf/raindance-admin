@extends('admin::layouts.master')

@section('title','操作日志')


@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <i-select slot="prepend" style="width: 100px" placeholder="请求方法" v-model="requestMethod">
                        <i-option value="POST">POST</i-option>
                        <i-option value="PUT">PUT</i-option>
                        <i-option value="DELETE">DELETE</i-option>
                    </i-select>

                    <i-input placeholder="用户ID" style="width: 100px;" v-model="userId"></i-input>
                    <i-input  style="width: 200px;" placeholder="请求路径" v-model="path"></i-input>
                    <i-button type="primary" icon="ios-search" @click="handleSearch">搜索</i-button>
                    <i-button type="success" icon="ios-sync" @click="refresh">重置</i-button>
                    <i-button type="error" icon="ios-trash-outline" @click="batchDelete">批量删除</i-button>
                </div>
                <div class="box-body">
                    <div class="table-content">
                        <i-table border :columns="columns" :data="data6" @on-selection-change="selectionChange"></i-table>
                        <page :total="total" :page-size="pageSize" @on-change="handlePaginate" show-total style="margin-top: 20px;"></page>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @stop
@section('javascript')
    <script>
        var vm = new Vue({
            el: '#app',
            data: {
                requestMethod: '',
                userId: '',
                path: '',
                total: 10,
                pageSize: 15,
                currentPage: 1,
                delIds: [],
                columns: [
                    {
                        type: 'selection',
                        width: 60,
                        align: 'center',

                    },
                    {
                        type: 'index',
                        title: '序号',
                        width: 80,
                        align: 'center'
                    },
                    {
                        title: '用户名',
                        width: 120,
                        align: 'center',
                        render: (h, params) => {
                            return h('span', {}, params.row.causer["name"])
                        }

                    },
                    {
                        title: '请求方法',
                        key: 'method',
                        width: 120,
                        align: 'center'

                    },
                    {
                        title: '路径',
                        key: 'path',
                        width: 260,
                        align: 'center'

                    },
                    {
                        title: 'IP地址',
                        key: 'ip',
                        width: 120,
                        align: 'center'
                    },
                    {
                        title: '模型',
                        key: 'subject_type',
                        width: 200,
                        align: 'center'
                    },
                    {
                        title: '数据ID',
                        key: 'subject_id',
                        width: 80,
                        align: 'center'
                    },
                    {
                        title: '描述',
                        key: 'description',
                        width: 200,
                        align: 'center'
                    },
                    {
                        title: '数据',
                        key: 'properties',
                        width: 200,
                        align: 'center'
                    },
                    {
                        title: '创建时间',
                        key: 'created_at',
                        width: 200,
                        align: 'center'
                    },
                    {
                        title: '操作',
                        key: 'action',
                        width: 150,
                        align: 'center',
                        render: (h, params) => {
                            return h('div', [
                                h('Button', {
                                    props: {
                                        type: 'error',
                                        size: 'small'
                                    },
                                    on: {
                                        click: () => {
                                            vm.remove(params)
                                        }
                                    }
                                }, '删除')
                            ]);
                        }
                    }
                ],
                data6: []

            },
            methods: {
                getData () {
                    const _this = this
                    axios.get('{{route("logs.all")}}?page=' +
                        this.currentPage + '&pageSize=' +
                        this.pageSize + '&requestMethod=' + this.requestMethod +
                            '&path=' + this.path +
                            '&userId=' + this.userId
                    ).then(res => {
                        if (res.data.code === 0) {
                            if (res.data.data instanceof Object) {
                                _this.total = res.data.data.total
                                if(res.data.data.data instanceof Array) {
                                    _this.data6 = res.data.data.data
                                }

                            }
                        }
                    })
                },
                handlePaginate (num) {
                    this.currentPage = num
                    this.getData()
                },
                //selectionChange
                selectionChange (selection) {
                    const _this = this
                    _this.delIds = []
                    selection.forEach(function (v) {
                        _this.delIds.push(v['id'])
                    })
                },
                //批量删除
                batchDelete () {
                    const _this = this
                    if (this.delIds.length === 0) {
                        this.$Message.info('请至少选择一条数据')
                        return
                    }
                    this.$Modal.confirm({
                        title: '确定要删除吗？',
                        onOk: () => {
                            const url = AdminTools.replaceUrl("{{route('logs.destroy', ['id' => '__param__'])}}", this.delIds)
                            axios.delete(url).then(res => {
                                if (res.data.code === 0) {
                                    _this.$Message.success('删除成功')
                                    setTimeout(function () {
                                        location.reload()
                                    }, 1500)
                                } else {
                                    _this.$Message.info(res.data.message)
                                }
                            })
                        },
                        onCancel: () => {
                            //this.$Message.info('Clicked cancel');
                        }
                    });
                },
                //检索
                handleSearch () {
                    this.getData()
                },
                remove (param) {
                    const _this = this
                    this.$Modal.confirm({
                        title: '确定要删除吗？',
                        onOk: () => {
                            const url = AdminTools.replaceUrl("{{route('logs.destroy', ['id' => '__param__'])}}", param.row.id)

                            axios.delete(url).then(res => {
                                if (res.data.code === 0) {
                                    _this.$Message.success('删除成功')
                                    setTimeout(function () {
                                        location.reload()
                                    }, 1500)
                                } else {
                                    _this.$Message.info(res.data.message)
                                }
                            })
                        },
                        onCancel: () => {
                            //this.$Message.info('Clicked cancel');
                        }
                    });
                },
                //刷新
                refresh () {
                    this.requestMethod = ''
                    this.userId = ''
                    this.path = ''
                    this.getData()
                }
            },
            created () {
                this.$Message.config({
                    top:100,
                    duration: 2
                })
                this.getData()
            }
        });
        AdminTools.highlight(1)
    </script>
    @stop