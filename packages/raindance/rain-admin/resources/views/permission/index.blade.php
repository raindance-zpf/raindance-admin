@extends('admin::layouts.master')

@section('title','权限管理')


@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <div class="margin-bottom">
                        <i-input style="width: 200px;" placeholder="名称" v-model="slug"></i-input>
                        <i-input style="width: 200px;"  placeholder="标识" v-model="name"></i-input>
                        <i-button type="primary" @click="search" icon="ios-search">搜索</i-button>
                        <i-button type="info" @click="reset" icon="ios-refresh">重置</i-button>
                        <i-button type="success" to="{{route('permissions.create')}}" icon="ios-add-circle-outline">新增</i-button>

                    </div>
                </div>
                <div class="box-body">
                    <div class="table-content">
                        <i-table border :columns="columns" :data="data6"></i-table>
                        <page :total="total" :page-size="pageSize" @on-change="handlePaginate" show-total style="margin-top: 20px;"></page>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @stop
@section('javascript')
    <script>
        var vm = new Vue({
            el: '#app',
            data: {
                slug: '',
                name: '',
                total: 10,
                pageSize: 15,
                currentPage: 1,
                columns: [
                    {
                        type: 'index',
                        title: '序号',
                        width: 70,
                        align: 'center'
                    },
                    {
                        title: '名称',
                        key: 'slug',
                        width: 200,
                        align: 'center'

                    },
                    {
                        title: '标识',
                        key: 'name',
                        width: 260,
                        align: 'center'

                    },
                    /*{
                        title: 'Guard Name',
                        key: 'guard_name',
                        width: 120,
                        align: 'center'
                    },*/
                    {
                        title: '创建时间',
                        key: 'created_at',
                        width: 200,
                        align: 'center'
                    },
                    {
                        title: '操作',
                        key: 'action',
                        width: 150,
                        align: 'center',
                        render: (h, params) => {
                            return h('div', [
                                h('Button', {
                                    props: {
                                        type: 'primary',
                                        size: 'small'
                                    },
                                    style: {
                                        marginRight: '5px'
                                    },
                                    on: {

                                        click: () => {
                                            vm.show(params)
                                        }
                                    }
                                }, '修改'),
                                h('Button', {
                                    props: {
                                        type: 'error',
                                        size: 'small'
                                    },
                                    on: {
                                        click: () => {
                                            vm.remove(params)
                                        }
                                    }
                                }, '删除')
                            ]);
                        }
                    }
                ],
                data6: []

            },
            methods: {
                getData () {
                    const _this = this
                    axios.get("{{route('permissions.withPagination')}}?page=" + this.currentPage + '&pageSize=' + this.pageSize + '&name=' + this.name + '&slug=' + this.slug).then(res => {
                        if (res.data.code === 0) {
                            if (res.data.data instanceof Object) {
                                _this.total = res.data.data.total
                                if(res.data.data.data instanceof Array) {
                                    _this.data6 = res.data.data.data
                                }

                            }
                        }
                    })
                },
                handlePaginate (num) {
                    this.currentPage = num
                    this.getData()
                },
                show (param) {
                    location.href = AdminTools.replaceUrl("{{route('permissions.edit', ['id' => '__param__'])}}", param.row.id)
                },
                remove (param) {
                    const _this = this
                    this.$Modal.confirm({
                        title: '确定要删除吗？',
                        onOk: () => {
                            const url = AdminTools.replaceUrl("{{route('permissions.destroy', ['id' => '__param__'])}}", param.row.id)
                            axios.delete(url).then(res => {
                                if (res.data.code === 0) {
                                    _this.$Message.success('删除成功')
                                    setTimeout(function () {
                                        location.reload()
                                    }, 1500)
                                } else {
                                    _this.$Message.info(res.data.message);
                                }
                            })
                        },
                        onCancel: () => {
                            //this.$Message.info('Clicked cancel');
                        }
                    });
                },
                //检索
                search () {
                    this.getData()
                },
                //reset
                reset () {
                    this.name = ''
                    this.slug = ''
                    this.getData()
                }
            },
            created () {
                this.$Message.config({
                    top:100,
                    duration: 2
                })
                this.getData()
            }
        });
        AdminTools.highlight(1)
    </script>
    @stop