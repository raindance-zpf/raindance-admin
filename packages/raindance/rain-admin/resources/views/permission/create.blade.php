@extends('admin::layouts.master')

@section('title','权限管理')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">新增权限</h3>
                </div>
                <div class="box-body">
                    <i-form :label-width="100" ref="formRel" :rules="ruleValidate" :model="formModel">
                        <div class="panel panel-bordered">
                            <div class="panel-body">
                                <form-item label="名称" prop="slug">
                                    <i-input v-model="formModel.slug" placeholder="输入名称"></i-input>
                                </form-item>
                                <form-item label="标识" prop="name">
                                    <i-input v-model="formModel.name" placeholder="输入标识，必填是英文字母"></i-input>
                                </form-item>
                                {{--<form-item label="Guard Name" prop="guard_name">
                                    <i-input v-model="formModel.guard_name" placeholder="Guard Name example 'web'"></i-input>
                                </form-item>--}}
                                <form-item>
                                    <i-button type="primary" @click="handleSubmit('formRel')" :loading="loading">确定</i-button>
                                    <i-button type="default" @click="goBack()">返回</i-button>
                                </form-item>
                            </div>
                        </div>
                    </i-form>
                </div>
            </div>
        </div>
    </div>
    @stop
@section('javascript')
    <script>
        var url = "{{route('permissions.store')}}"
        var home = "{{route('permissions.index')}}"
        var vm = new Vue({
            el: '#app',
            data: {
                loading: false,
                formModel: {
                    name: '',
                    guard_name: 'admin'
                },
                ruleValidate: {
                    name: [
                        {required: true, message: '必填', trigger: 'blur'}
                    ],
                    slug: [
                        {required: true, message: '必填', trigger: 'blur'}
                    ]
                }
            },
            methods: {
                goBack: function () {
                    history.back(-1)
                },
                handleSubmit (name) {
                    const _this = this
                    this.$refs[name].validate(function (valid) {
                        if (valid) {
                            _this.loading = true
                            axios.post(url, vm.$data.formModel).then(function (res) {
                                if (res.data.code === 0) {
                                    vm.$Message.success('操作成功.');
                                    setTimeout(function () {
                                        location.href = home
                                    }, 1500)
                                } else {
                                    _this.loading = false
                                    vm.$Message.error(res.data.message);
                                }
                            })
                        } else {
                            //vm.$Message.error('Fail!');
                        }
                    })
                }
            }
        })
        AdminTools.highlight(1)
    </script>
    @endsection