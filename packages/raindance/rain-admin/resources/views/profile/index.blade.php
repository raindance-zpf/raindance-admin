@extends('admin::layouts.master')

@section('title','个人信息')
@section('style')
    <style>
        .demo-upload-list{
            display: inline-block;
            width: 60px;
            height: 60px;
            text-align: center;
            line-height: 60px;
            border: 1px solid transparent;
            border-radius: 4px;
            overflow: hidden;
            background: #fff;
            position: relative;
            box-shadow: 0 1px 1px rgba(0,0,0,.2);
            margin-right: 4px;
        }
        .demo-upload-list img{
            width: 100%;
            height: 100%;
        }
        .demo-upload-list-cover{
            display: none;
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            background: rgba(0,0,0,.6);
        }
        .demo-upload-list:hover .demo-upload-list-cover{
            display: block;
        }
        .demo-upload-list-cover i{
            color: #fff;
            font-size: 20px;
            cursor: pointer;
            margin: 0 2px;
        }
    </style>
    @stop



@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">个人信息</h3>
                </div>
                <div class="box-body">
                    <div class="col-md-8">
                        <i-form :label-width="100" ref="formRel" :rules="ruleValidate" :model="formModel">
                            <div class="panel panel-bordered">
                                <div class="panel-body">
                                    <form-item label="用户名" prop="name">
                                        <i-input v-model="formModel.name" placeholder="输入用户名"></i-input>
                                    </form-item>
                                    <form-item label="手机号码">
                                        <i-input v-model="formModel.mobile" placeholder="输入手机号码"></i-input>
                                    </form-item>
                                    <form-item label="密码" prop="password">
                                        <i-input v-model="formModel.password" type="password" placeholder="密码为空则不修改"></i-input>
                                    </form-item>
                                    <form-item label="确认密码" prop="password_confirmation">
                                        <i-input v-model="formModel.password_confirmation" type="password" placeholder="输入确认密码"></i-input>
                                    </form-item>
                                    <form-item>
                                        <Upload
                                                action="/admin/upload/image"
                                                :on-success="handleSuccess"
                                                :format="['jpg','jpeg','png']"
                                                :on-exceeded-size="handleMaxSize"
                                                :before-upload="beforeUpload"
                                                :on-format-error="handleFormatError"
                                                :show-upload-list="false"
                                                :max-size="1024">
                                            <i-button icon="ios-cloud-upload-outline">上传图片</i-button>
                                        </Upload>
                                        <div class="demo-upload-list" v-if="formModel.avatar">
                                            <img :src="formModel.avatar">
                                            <div class="demo-upload-list-cover">
                                                <Icon type="ios-eye-outline" @click.native="handleView(formModel.avatar)"></Icon>
                                                <Icon type="ios-trash-outline" @click.native="handleRemove(formModel.avatar)"></Icon>
                                            </div>
                                        </div>
                                    </form-item>
                                    <form-item>
                                        <i-button type="primary" @click="handleSubmit('formRel')" :loading="loading">确定</i-button>
                                        <i-button type="default" @click="goBack()">返回</i-button>
                                    </form-item>
                                </div>
                            </div>
                        </i-form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @stop
@section('javascript')
    <script>
        const vm = new Vue({
            el: '#app',
            data() {
                const checkRepassword = function (rule, value, callback) {
                    if (vm.$data.formModel.password != '' && value == '') {
                        callback(new Error('确认密码不能为空'))
                    } else if (vm.$data.formModel.password != vm.$data.formModel.password_confirmation) {
                        callback(new Error('确认密码与密码不一致'))
                    } else {
                        callback();
                    }
                }
                return {
                    loading: false,
                    formModel: {
                        name: '',
                        password: '',
                        password_confirmation: '',
                        avatar: '',
                        avatar_path: '',
                        mobile: ''
                    },
                    ruleValidate: {
                        name: [
                            {required: true, message: '必填', trigger: 'blur'}
                        ],
                        password: [
                            {required: false, message: '必填', trigger: 'blur'}
                        ],
                        password_confirmation: [
                            {required: false, validator: checkRepassword, trigger: 'blur'}
                        ]
                    }
                }

            },
            methods: {
                goBack: function () {
                    history.back(-1)
                },
                //文件超过限制
                handleMaxSize (file, fileList) {
                    this.$Notice.warning({
                        title: '超过文件大小限制',
                        desc: '文件  ' + file.name + ' 太大，已经超过1M.'
                    });
                },
                //文件格式错误
                handleFormatError (file, fileList) {
                    this.$Message.error('上传文件格式不正确.');
                },
                //上传前handle
                beforeUpload () {
                    if (this.formModel.avatar !== '' && this.formModel.avatar !== null && this.formModel.avatar !== undefined) {
                        this.$Message.error('只能上传一张图片.');
                        return false;
                    }
                },
                //上传成功handle
                handleSuccess (res, file) {
                    if (res.status === 'success') {
                        this.formModel.avatar = res.data
                    }
                },
                //图片预览
                handleView (url) {
                    this.$Modal.confirm({
                        render: (h) => {
                            return h('img', {
                                attrs: {
                                    src: url
                                },
                                style: {
                                    width: '100%'
                                }
                            })
                        }
                    })
                },
                //删除图片
                handleRemove (filename) {
                    const _this = this
                    this.$Modal.confirm({
                        title: '确定要删除吗？',
                        onOk: () => {
                            axios.put("{{route('upload.remove')}}", {filename: filename}).then(res => {
                                if (res.data.code === 0) {
                                    _this.formModel.avatar = ''
                                    _this.formModel.avatar_path = ''
                                    _this.$Message.success('删除成功')
                                } else {
                                    _this.$Message.info(res.data.message);
                                }
                            })
                        },
                        onCancel: () => {
                            //this.$Message.info('Clicked cancel');
                        }
                    })
                },
                handleSubmit (name) {
                    const _this = this
                    this.$refs[name].validate(function (valid) {
                        if (valid) {
                            _this.loading = true
                            axios.put("{{route('profile.update')}}", vm.$data.formModel).then(function (res) {
                                if (res.data.code === 0) {
                                    vm.$Message.success('操作成功.');
                                    setTimeout(function () {
                                        location.reload()
                                    }, 1500)
                                } else {
                                    _this.loading = false
                                    vm.$Message.error(res.data.message);
                                }
                            }).catch(err => {
                                _this.loading = false
                                if(err.response.status == 422) {
                                    let emsg = null
                                    if ((emsg = AdminTools.axiosCatchError(err.response.data.errors))) {
                                        _this.$Message.error(emsg)
                                    }
                                }
                            })
                        } else {
                            //vm.$Message.error('Fail!');
                        }
                    })
                },
                getInfo () {
                    const _this = this
                    axios.get("{{route('profile.info')}}").then(res => {
                        if (res.data.code === 0) {
                            _this.formModel = res.data.data
                        }
                    })
                }
            },
            created () {
                this.getInfo()
                this.$Message.config({
                    top:100,
                    duration: 2
                })
            }
        })
        AdminTools.highlight(1)

    </script>
    @endsection