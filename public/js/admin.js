window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
}
window.axiosService = new axios.create();
//添加一个请求拦截器
/*axios.interceptors.request.use(function(config){
    //在请求发出之前进行一些操作
    return config;
},function(err){
    //Do something with request error
    return Promise.reject(error);
});*/

//添加一个响应拦截器
/*
window.axiosService.interceptors.response.use(function(res){
    //在这里对返回的数据进行处理
    console.log(res)
    return res.data;
},function(err){
    //Do something with response error
    return Promise.reject(error);
})*/
