<?php

namespace App\Providers;

use App\Repositories\Impl\MenuRepositoryEloquent;
use App\Repositories\Impl\UserRepositoryEloquent;
use App\Repositories\MenuRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
       //
    }
}
