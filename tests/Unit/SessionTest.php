<?php

namespace Tests\Unit;

use App\User;
use Raindance\RainAdmin\Services\DictService;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SessionTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->withSession(['foo' => 'bar'])
            ->get('/');
        $response->assertSessionHas('foo');

    }
    /*public function testUserSession ()
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)
            ->withSession(['foo' => 'bar'])
            ->get('/');
        $this->assertAuthenticated();
    }*/

    public function testDict ()
    {
        $list = DictService::getDictToArray('gender');
        $this->assertCount(2, $list);
    }
}
