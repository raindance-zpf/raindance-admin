-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- Server version:               5.7.25 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL 版本:                  10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table raindance-admin.admin_logs
CREATE TABLE IF NOT EXISTS `admin_logs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT 'admin_users fk',
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'URL路径',
  `method` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '方法',
  `model` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '模型名称',
  `model_id` bigint(20) NOT NULL COMMENT '数据ID',
  `ip` char(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP',
  `remark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Remark',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='操作日志';

-- Dumping data for table raindance-admin.admin_logs: ~0 rows (approximately)
DELETE FROM `admin_logs`;
/*!40000 ALTER TABLE `admin_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_logs` ENABLE KEYS */;

-- Dumping structure for table raindance-admin.admin_users
CREATE TABLE IF NOT EXISTS `admin_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'UUID',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Name',
  `mobile` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '手机号',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Password',
  `remember_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Email',
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '头像',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table raindance-admin.admin_users: ~2 rows (approximately)
DELETE FROM `admin_users`;
/*!40000 ALTER TABLE `admin_users` DISABLE KEYS */;
INSERT INTO `admin_users` (`id`, `uuid`, `name`, `mobile`, `password`, `remember_token`, `email`, `avatar`, `created_at`, `updated_at`) VALUES
	(1, 'ac7f348809f54e40ae50219400711249', 'admin', '13848636200', '$2y$10$mfeFsBfGb5X1qtft5U0Q2e9ZrRUbrbEavIG.3na82uwtOwJQ5EnfK', 'wD5CgkdVI7QDIBc1diBjj5A1UBoZUkMd973kRKFDuHuDSwzNT1dn30C1TWWT', 'admin@admin.com', 'http://raindance-admin.test/uploads/images/67e6015ab36854e75b7e68f08fe1007d.jpg', '2019-01-23 18:02:01', '2019-03-14 15:13:12'),
	(3, '1a4ba161f0884e82aebd05db4f04804d', 'zpf', '15598426200', '$2y$10$LWrSnPChTOF3WlkXwOxfUO6liZCayFWysD5z.S2sjENEPFF7GsV7y', '6NXlTgWkoVWyevQRNhPSBGZrRmktfxzU3Ls9NuUc4RLEYoQX3RI65IBorhCi', NULL, 'http://raindance-admin.test/uploads/images/3fc3084d1ec797154e5e01136f8adb6f.jpg', '2019-01-24 11:43:40', '2019-03-14 18:12:16');
/*!40000 ALTER TABLE `admin_users` ENABLE KEYS */;

-- Dumping structure for table raindance-admin.dictionaries
CREATE TABLE IF NOT EXISTS `dictionaries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL COMMENT 'parent id ',
  `lft` int(11) DEFAULT NULL COMMENT 'left bound',
  `rgt` int(11) DEFAULT NULL COMMENT 'right bound',
  `dict_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '字典名称',
  `dict_label` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '字典标签',
  `dict_value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '字典值',
  `dict_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '字典类型',
  `remark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '备注',
  `sort_order` int(11) NOT NULL DEFAULT '0' COMMENT '排序越小越靠前',
  `is_system` tinyint(4) NOT NULL DEFAULT '1' COMMENT '系统内置0-不是1-是',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '状态0-停用1-正常',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table raindance-admin.dictionaries: ~3 rows (approximately)
DELETE FROM `dictionaries`;
/*!40000 ALTER TABLE `dictionaries` DISABLE KEYS */;
INSERT INTO `dictionaries` (`id`, `parent_id`, `lft`, `rgt`, `dict_name`, `dict_label`, `dict_value`, `dict_type`, `remark`, `sort_order`, `is_system`, `status`, `created_at`, `updated_at`) VALUES
	(2, NULL, 1, 6, '性别', NULL, NULL, 'gender', '1男2女3未知', 0, 1, 1, '2019-03-15 15:11:33', '2019-03-22 10:02:43'),
	(4, 2, 2, 3, NULL, '男', '1', 'gender', '男', 1, 1, 1, '2019-03-21 16:44:03', '2019-03-22 09:33:51'),
	(5, 2, 4, 5, NULL, '女', '2', 'gender', '女', 2, 1, 1, '2019-03-21 17:00:55', '2019-03-22 09:34:04'),
	(7, NULL, 7, 8, '学历列表', NULL, NULL, 'xueli', NULL, 0, 1, 1, '2019-03-22 10:15:59', '2019-03-22 10:15:59');
/*!40000 ALTER TABLE `dictionaries` ENABLE KEYS */;

-- Dumping structure for table raindance-admin.menus
CREATE TABLE IF NOT EXISTS `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='菜单';

-- Dumping data for table raindance-admin.menus: ~7 rows (approximately)
DELETE FROM `menus`;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` (`id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `lft`, `rgt`, `sort_order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
	(1, '系统设置', '#', '_self', ' fa-gears', NULL, NULL, 1, 16, 99, '2019-01-25 15:07:37', '2019-02-15 12:03:11', NULL, NULL),
	(2, '用户', 'admin/users', '_self', 'fa-users', NULL, 1, 2, 3, 0, '2019-01-25 15:08:11', '2019-03-12 16:41:22', 'users.index', NULL),
	(3, '角色', 'admin/roles', '_self', 'fa-server', NULL, 1, 4, 5, 0, '2019-01-25 16:50:47', '2019-03-12 16:42:22', 'roles.index', NULL),
	(4, '权限', 'admin/permissions', '_self', 'fa-ellipsis-h', NULL, 1, 6, 7, 0, '2019-01-25 16:51:53', '2019-03-12 16:42:40', 'permissions.index', NULL),
	(5, '菜单', 'admin/menus', '_self', 'fa-bars', NULL, 1, 8, 9, 0, '2019-02-15 12:01:30', '2019-03-12 16:42:53', 'menus.index', NULL),
	(6, '操作日志', 'admin/logs', '_self', 'fa-database', NULL, 1, 10, 11, 5, '2019-03-13 15:30:43', '2019-03-13 15:30:43', NULL, NULL),
	(7, 'Log-viewer', 'admin/log-viewer', '_self', 'fa-info-circle', NULL, 1, 12, 13, 6, '2019-03-13 17:18:17', '2019-03-13 17:18:17', NULL, NULL),
	(8, 'Dashboard', 'admin/dashboard', '_self', 'fa-laptop', NULL, NULL, 17, 18, 0, '2019-03-14 16:12:50', '2019-03-14 16:12:50', NULL, NULL),
	(9, '字典', 'admin/dict', '_self', 'fa-barcode', NULL, 1, 14, 15, 0, '2019-03-15 10:32:55', '2019-03-15 11:26:45', NULL, NULL);
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;

-- Dumping structure for table raindance-admin.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table raindance-admin.migrations: ~4 rows (approximately)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2019_01_21_035226_create_admin_users_table', 2),
	(4, '2019_01_21_060144_create_permission_tables', 2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table raindance-admin.model_has_permissions
CREATE TABLE IF NOT EXISTS `model_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table raindance-admin.model_has_permissions: ~0 rows (approximately)
DELETE FROM `model_has_permissions`;
/*!40000 ALTER TABLE `model_has_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `model_has_permissions` ENABLE KEYS */;

-- Dumping structure for table raindance-admin.model_has_roles
CREATE TABLE IF NOT EXISTS `model_has_roles` (
  `role_id` int(10) unsigned NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table raindance-admin.model_has_roles: ~2 rows (approximately)
DELETE FROM `model_has_roles`;
/*!40000 ALTER TABLE `model_has_roles` DISABLE KEYS */;
INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
	(2, 'Raindance\\RainAdmin\\Models\\AdminUser', 1),
	(3, 'Raindance\\RainAdmin\\Models\\AdminUser', 3);
/*!40000 ALTER TABLE `model_has_roles` ENABLE KEYS */;

-- Dumping structure for table raindance-admin.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table raindance-admin.password_resets: ~0 rows (approximately)
DELETE FROM `password_resets`;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table raindance-admin.permissions
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table raindance-admin.permissions: ~6 rows (approximately)
DELETE FROM `permissions`;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` (`id`, `name`, `slug`, `guard_name`, `created_at`, `updated_at`) VALUES
	(2, 'roles.create', '添加角色', 'admin', '2019-01-22 16:32:17', '2019-02-15 16:58:11'),
	(3, 'roles.destory', '删除角色', 'admin', '2019-01-23 10:39:05', '2019-02-15 16:58:01'),
	(4, 'roles.index', '管理加色', 'admin', '2019-02-15 16:57:11', '2019-02-15 16:57:53'),
	(5, 'users.index', '管理用户', 'admin', '2019-02-15 16:57:41', '2019-02-15 16:57:41'),
	(6, 'permissions.index', '管理角色', 'admin', '2019-02-15 16:58:55', '2019-02-15 16:58:55'),
	(7, 'menus.index', '管理菜单', 'admin', '2019-02-15 16:59:11', '2019-03-12 14:42:02');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;

-- Dumping structure for table raindance-admin.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table raindance-admin.roles: ~2 rows (approximately)
DELETE FROM `roles`;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `name`, `guard_name`, `slug`, `created_at`, `updated_at`) VALUES
	(2, 'super-admin', 'admin', '超级管理员', '2019-01-22 11:09:53', '2019-03-15 10:05:00'),
	(3, 'general-users', 'admin', '普通用户', '2019-01-22 17:00:11', '2019-04-08 09:48:26');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping structure for table raindance-admin.role_has_permissions
CREATE TABLE IF NOT EXISTS `role_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`),
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table raindance-admin.role_has_permissions: ~4 rows (approximately)
DELETE FROM `role_has_permissions`;
/*!40000 ALTER TABLE `role_has_permissions` DISABLE KEYS */;
INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
	(2, 2),
	(3, 2),
	(2, 3),
	(3, 3);
/*!40000 ALTER TABLE `role_has_permissions` ENABLE KEYS */;

-- Dumping structure for table raindance-admin.role_menus
CREATE TABLE IF NOT EXISTS `role_menus` (
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  KEY `role_menus_role_id_index` (`role_id`),
  KEY `role_menus_menu_id_index` (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table raindance-admin.role_menus: ~2 rows (approximately)
DELETE FROM `role_menus`;
/*!40000 ALTER TABLE `role_menus` DISABLE KEYS */;
INSERT INTO `role_menus` (`role_id`, `menu_id`) VALUES
	(3, 7),
	(3, 8);
/*!40000 ALTER TABLE `role_menus` ENABLE KEYS */;

-- Dumping structure for table raindance-admin.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table raindance-admin.users: ~2 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `uuid`, `created_at`, `updated_at`) VALUES
	(1, 'Javier Treutel Jr.', 'collins.joshuah@example.org', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'tabJa2VROI', NULL, '2019-03-22 14:27:49', '2019-03-22 14:27:49'),
	(2, 'Mr. Cary Corkery DDS', 'berenice.hermiston@example.org', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'lIlxfzgKQk', NULL, '2019-03-22 14:34:45', '2019-03-22 14:34:45'),
	(3, 'Nelle Harris', 'qmorissette@example.org', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'X6IB7pNn8z', NULL, '2019-03-22 14:40:23', '2019-03-22 14:40:23');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
